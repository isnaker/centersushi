<?php
$app = new App();
?>
<div id="summerTime"></div>
<footer>

    <div class="box33p">
        <img style="height: 90px; margin: 10px; float: left" src="img/logo.png" alt="Центр суши">
        <p><strong>Наш адрес:</strong>
            <?php  $app->address();  ?></p>
        <p class="phones"><strong>Наш телефон:</strong>
            <strong><a href="tel:/<?php  $app->phone(); ?>"><?php  $app->phone(); ?></a></strong>
        </p>


        <div id="counters">
            <!-- Yandex.Metrika informer -->
            <a href="https://metrika.yandex.ru/stat/?id=24859694&amp;from=informer"
               target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/24859694/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                                                   style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:24859694,lang:'ru'});return false}catch(e){}"/></a>
            <!-- /Yandex.Metrika informer -->

            <!-- Yandex.Metrika counter -->
            <script type="text/javascript">
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function() {
                        try {
                            w.yaCounter24859694 = new Ya.Metrika({id:24859694,
                                webvisor:true,
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true});
                        } catch(e) { }
                    });

                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else { f(); }
                })(document, window, "yandex_metrika_callbacks");
            </script>
            <noscript><div><img src="//mc.yandex.ru/watch/24859694" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->
            <script src="https://use.fontawesome.com/953328cc22.js"></script>
        </div>
    </div>
    <div class="box33p">
        <p style="color: #a6a6a6; font-size: 0.6em; text-align: center">Делая заказ на сайте Center-Sushi.ru, вы подтверждаете согласие с условиями работы и соглашаетесь в получении информационных смс-сообщений об акциях и новостях компании.
                Смс - информирование может быть отключено в любой момент путем устного заявления клиента.</p>
    </div>




    <div id="creator" style="float: right; display: inline-block; margin: 15px;">
        <a href="http://www.snapix.ru" target="_blank"><img src="img/snapix.png" alt="Snapix - студия IT-решений для бизнеса"> </a>
    </div>


</footer>
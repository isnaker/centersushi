<?php
require('engine/config.php');
$app = new App();
$db = new DB();
?>

<html>
<head>
    <title>Доставка суши и ролов в Коломне. Контакты. Центр Суши</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/product.css" type="text/css">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="css/product.preview.css" type="text/css">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="keywords" content="Центр Суши, суши, роллы, заказать, заказать роллы Коломна, роллы Коломна, суши Коломна, заказать суши Коломна, заказать роллы, заказать суши, Суши ,доставка, суши, роллы, салаты, сашими, сеты, горячее, супы, десерты, напитки, Коломна, японская кухня, блюда, суши в коломне, доставка суши в коломне, заказать суши в Коломне"/>
    <meta name="description" content="Бесплатная доставка суши и роллов на дом в Коломне. Японские блюда от «Центр Суши» с доставкой."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/basic.js"></script>
    <script type="text/javascript" src="js/cart.js"></script>
    <style type="text/css">
        #map-canvas {
            width: 100%;
            height: 270px;
            background: white;
        }
        .contacts h4 {
            margin: 1%;
            margin-left: 2%;
        }
    </style>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=ru"></script>
    <script type="text/javascript">
        function initialize(){
            var centr = new google.maps.LatLng(55.084006, 38.777553);
            var map;
            var opts = {
                zoom: 12,
                center: centr,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggableCursor: 'default',
                disableDefaultUI: false
            }

            map = new google.maps.Map(document.getElementById("map-canvas"), opts);
            var image = 'img/icon.png';
            var marker = new google.maps.Marker({
                map: map,
                position: centr,
                icon: image
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('nav a[href="contacts.php"]').addClass('current');
            $('.blocklist li').hide();
            $("#left-column .expander").click(function(){
                $(this).toggleClass('expanded');
                var p = $(this).parent().next();
                if ($(this).hasClass('expanded')){
                    $(p).find('li').show(300);
                } else {
                    $(p).find('li').hide(500);
                }
            });
        });
    </script>
</head>

<body>
<?php include_once('header.php'); ?>
<div id="content">
    <div id="left-column">
        <div class="accordion">
            <?php
            $categories = $db->getCategories("",true);
            $products = $db->getProducts();

            foreach ($categories as $category){
                $products = $db->getProducts("`category` = $category->id","","`name`");
                if (count($products) > 0){
                    print ("<span class='blocklist-head'><span class='expander'>$category->name</span> <a href='category.php?category=".$category->id."'>Все</a> </span><ul class='blocklist'>");
                    foreach ($products as $product){
                        print "<li><a href='product.php?id=$product->id'>".$product->name."<span data-value='$product->id' class='quickAddToCart'>+</span></a></li>";
                    }
                    print ("</ul>");
                }
            }
            ?>
        </div>
    </div>
    <div id="right-column">
        <div class='breadcrumbs'>
            <a href='index.php'>Центр суши</a> >
            <span>Контакты</span>
        </div>
        <div id="main">
            <div id="banners" style="float: right;width: 200px">
            <a title="Группа Центр Суши Вконтакте" style="float: right" href="http://vk.com/centr_sushi_kolomna" target="_blank"><img src="img/vk.png" alt="Группа Центр Суши Вконтакте"> </a>
            </div>
            <h1>Контакты</h1>
            <div class="contacts">
                Адрес: <h4><?php $app->address(); ?></h4>
                Телефоны: <h4><a href="tel:<?php $app->phone();  ?>"><?php $app->phone(); ?></a></h4>
                Режим работы:
                        <h4>Понедельник - Черверг: с 11:00 до 23:00</h4>
                        <h4>Пятница - Суббота: с 11:00 до 00:00</h4>
                        <h4>Воскресенье: с 11:00 до 23:00</h4>
            </div>
            <h4 style="margin: 40px 0 4px 0">Мы на карте</h4>
            <div id="map-canvas"></div>
        </div>
    </div>
</div>

<?php include_once('footer.php'); ?>
</body>
</html>
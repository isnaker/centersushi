<?php
include('engine/Cart.php');
$db = new DB();

function sortl($a, $b, $sortByField = 'id') {if($a->id === $b->id)return 0;return $a->id < $b->id ? 1 : -1;}
//uasort($products, 'myCmp');  // сортировка элементов
$counter = 0;
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Корзина. Доставка роллов в Коломне. Заказать суши в Коломне. Центр Суши</title>
    <link rel="stylesheet" href="css/framework.css" type="text/css">
    <link rel="stylesheet" href="css/product.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link type="text/css" rel="stylesheet" href="css/flexslider.css">

    <link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name='yandex-verification' content='48de752d2a7ce431' />
    <meta name="keywords" content="Центр Суши, суши, роллы, заказать, заказать роллы Коломна, роллы Коломна, суши Коломна, заказать суши Коломна, заказать роллы, заказать суши, Суши ,доставка, суши, роллы, салаты, сашими, сеты, горячее, супы, десерты, напитки, Коломна, японская кухня, блюда, суши в коломне, доставка суши в коломне, заказать суши в Коломне"/>
    <meta name="description" content="Японские блюда от «Центр Суши» с доставкой. Доставка суши и роллов на дом в Коломне."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/basic.js"></script>
    <script type="text/javascript" src="js/cart.js"></script>
    <script type="text/javascript" src="js/jquery.easing.js"></script>
    <script type="text/javascript" src="js/modernizr.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">


        function createOrder(data, sender){
            response = sendAjax('createOrder', data);
            $('#'+sender).html('Заказ создан!');
        }

        function printResult(resultid){
            yaCounter24859694.reachGoal('Make_order');
            getCart();
            $('#cancelOrdering').html('Закрыть окно');
            $('#createOrderButton').hide(500);
            $('#orderbox_body').html("Ваш заказ успешно создан!<br><h3>" + resultid +"</h3>" + " <span>номер Вашего заказа</span><br>" +
                "В ближайшее время наш менеджер свяжется с Вами!");
            $('#cart-page').html("");
            $('#cart-page').append('<div id="dummy_cartIsEmpty" style="display: block;">'+
                '<p class="container text-centered" style="font-size: 46px">Ваша корзина пуста.</p>'+
            '<div class="text-centered"><a class="centered minimalicbutton" href="category.php">Заказать что-нибудь</a></div>'+
            '</div>');
        }

        function printProductList(){
            eraseCartContainers();
            cart.total = 0;
            cart.clearTotal = 0;
            for (var i=0;i<cart.items.length;i++){
                var clearTotal = parseInt(cart.items[i].product.price * cart.items[i].count);
                var total = parseInt(clearTotal * (cart.items[i].multiplicator));
                $('#product_list').append("<div class='cart-item text-centered'>" +
                    "<div class='removeIcon' data-value='"+cart.items[i].product.id+"'></div>" +
                    "<img class='item-image' src='/img/products/300/"+ cart.items[i].product.image +"' alt='$product->name'>" +
                    "<h3>"+cart.items[i].product.name+"</h3>" +
                    "<span>"+cart.items[i].product.price+" <span class='rub'>c</span> </span>" +
                    "<div class='item-count'> " +
                    "<span class='decCount' data-value='"+ cart.items[i].product.id +"'> - </span> " +
                    "<span class='Count'>" + cart.items[i].count + "</span>" +
                    "<span class='incCount' data-value='"+ cart.items[i].product.id +"'> + </span> </div>" +
                    "<div class='item-total'>" + total + "<span class='rub'>c</span></div>" +
                    "</div>");
                cart.clearTotal +=clearTotal;
                cart.total += total;
            }
            if (cart.items.length > 0){
                var confirmbutton = "<div id='order_too_small'>Минимальный заказ - 450 <span class='rub'>c</span></div>";
                if (cart.clearTotal >= 450){
                    confirmbutton = "<button id='completeOrderButton' class='minimalicbutton'>Оформить заказ</button>";
                }

            $('#dummy_cartIsEmpty').css('display','none');
            $('#cart-page').append("<div id='total-container'>" +
                "<h3>Итого:</h3>"+
            "<div id='order-total'>"+cart.total+"<strong class='black'><span class='rub'>c</span></strong></div><div id='buttons'>" +
                confirmbutton+
            "<button id='clearCartButton' class='minimalicbutton'>Очистить корзину</button>"+
            "</div></div>");
            } else {$('#dummy_cartIsEmpty').css('display','block');}

            $('.removeIcon').click(function(){
                var id = $(this).attr('data-value');
                removeProduct(id);
                // send callback to  $(this).parent();
            });

            $('.incCount').click(function(){
                var id = $(this).attr('data-value');
                sendAjax("updateCart", {id:id,delta:1});
            });

            $('.decCount').click(function(){
                var id = $(this).attr('data-value');
                sendAjax("updateCart", {id:id,delta:-1});
            });

            $('#clearCartButton').click(function(){ eraseCart(); });

            $('#cancelOrdering').click(function(){
                $('#orderbox').removeClass('showed');

                $('#orderbox').css('top','-500px');
                $('#overlay').removeClass('showed');
               // updateOrderboxPosition();
            });

            $('#completeOrderButton').click(function(){
                $('#orderbox').addClass('showed');

              //  $('#orderbox').css('top','-500px');
                $('#overlay').addClass('showed');
                updateOrderboxPosition();
            });
        }

        function updateOrderboxPosition(){
            var wd = ($(document).width() / 2) - ($('#orderbox').width() / 2);
            var hh = ($(window).height() / 2 ) - ($('#orderbox').height() / 2);
            $('#orderbox.showed').css('left',wd+"px").css('top',hh+'px');
        }

        $(window).resize(function(){
            updateOrderboxPosition();
        });

        $(document).ready(function(){
            $('input[type="tel"]').mask('8 (999) 999-99-99');
            getCart('printProductList()');
            $('nav a[href="cart.php"]').addClass('current');
            $('.blocklist li').hide();
            $('#menu_controller').click(function(){
                $('#left-column .accordion').toggleClass('showed');
            });

            $("#left-column .expander").click(function(){
                $(this).toggleClass('expanded');
                var p = $(this).parent().next();
                if ($(this).hasClass('expanded')){
                    $(p).find('li').show(300);
                } else {
                    $(p).find('li').hide(500);
                }
            });

            $('#createOrderButton').click(function(){
                    //validate data
                var validated = true;
                var data = {
                    name : $('#order_clientname').val(),
                    phone : $('#order_phone').val(),
                    email: $('#order_email').val(),
                    address: $('#order_address').val(),
                    comment: $('#order_comment').val()
                }

                if (data.name == ""){validated=false;}
                if (data.phone == ""){validated=false;}
                if (data.address == ""){validated=false;}

                if (validated){ createOrder(data,this.id); }
            });

        });
    </script>
    <style type="text/css">
        #cart {background: rgba(232, 10, 0, 0.50);padding: 4px}
        .cart-item {background:#ffffff;display: inline-block;padding-bottom: 15px; width: 31%; margin: 1%; float: left;border-radius: 6px;overflow: hidden; transition:all 0.3s ease;-webkit-transition:all 0.3s ease}
        .cart-item:hover {box-shadow: 0 2px 6px rgba(0,0,0,0.33)}
        .item-image {width: 100%;border-radius: 6px 6px 0 0;}
        .cart-item h3 {min-height: 50px; line-height: 1.1;width: 90%;margin: 0 5%}
        .item-count {margin-top: 10px;clear: both;display: block}
        .item-total {color: #e6373c;font-size: 36px}
        .black {color: black ; font-size: 28px}

        .removeIcon {width: 23px; height: 23px; background: url('img/close_icon.png') no-repeat;position: absolute;margin: 6px;transition:all 0.3s ease;-webkit-transition:all 0.3s ease}
        .removeIcon:hover {background-position: bottom center;cursor: pointer}
        #cart-page {width: 100%;float: left;display: inline-block;}
        #product_list {width: 66%;float: left}
        #total-container {float: right;display: inline-block; margin-left: 15px;position: fixed}
        #order-total {font-size: 46px;color: #e6373c;padding-left: 10px}

        #completeOrderButton {}
        #minicart_productCount {transition:all 0.2s ease;-webkit-transition:all 0.2s ease;}


        .decCount, .incCount {
            border: #e6373c solid 1px;
            border-radius: 50%;
            width: 21px;
            text-align: center;
            padding: 0px;
            padding-top: 5px;
            padding-bottom: 7px;
            line-height: 0.5;
            display: inline-block;
            transition: all 0.2s ease;
            -webkit-transition: all 0.2s ease;
        }
        .decCount {margin-right: 2px}
        .incCount {margin-left: 5px}

        .Count {font-size: 22px;margin: 0 3px}
        .decCount:hover, .incCount:hover {
            cursor: pointer;
            background: #e6373c;
            color: #ffffff;
        }

        #overlay {
            display: none;

            position: fixed; top: 0; bottom: 0; left: 0; right: 0; background: rgba(0,0,0,0.2); z-index: 8;
            transition:all 0.2s ease;
            -webkit-transition:all 0.2s ease;}

        #overlay.showed {display: block; z-index: 99}
        #orderbox {
            z-index: 100;
            text-align: left;
            position: absolute;
            top: -550px;
            margin: 0 auto;
            display: block;
            width: 480px;
            padding: 14px 25px 20px 25px;
            background: url(/img/logo.png) no-repeat;
            background-position: right top;
            background-color: #ffffff;
            border: silver solid 1px;
            box-shadow: 0 1px 4px rgba(0,0,0,0.33);
            transition:all 0.8s ease;
            -webkit-transition:all 0.8s ease;
        }

        #orderbox.showed {}

        #orderbox .label {
            display: block;
            font-size: 14px;
            margin-top: 3px;
        }

        #orderbox .heading {font-size: 28px; font-weight: bold; margin: 0 0 20px 0;display: block}
        #orderbox_body {min-height: 120px; width: 60%}
        #orderbox_body h3 {
            display: inline-block;
            font-size: 46px;
        }
        .input {border: silver solid 1px; border-radius: 2px; padding: 4px 6px;  box-shadow: 0 1px 3px rgba(0,0,0,0.35)}
        #order_address {width: 60%}

        #order_too_small {
            max-width: 100%;
            text-align: center;
            font-size: 0.75em;
            margin: 2px 0 2em 0;

        }

        /* smartphones */
        @media only screen
        and (min-device-width : 320px)
        and (max-device-width : 480px) {
            #mobile_nav_toggle {
                bottom: 10%;
                right: -2%;
            }
            #orderbox {
                left: 5% !important;
                width: 82%;
                padding: 2% 4%;
                margin: 0;
                background-size: 30%;
                background-position: right 25px;
                position: fixed;
            }
            .minimalicbutton {min-width: 46%}
            #total-container {
                float: right;
                display: block;
                margin-left: 0;
                position: fixed;

                bottom: 0;
                background: white;
                width: 92%;
                padding: 0% 4% 4% 4%;
                box-shadow: 0 1px 6px rgba(0,0,0,0.77);
            }
            #total-container h3 {display: inline;}
            #order-total {
                font-size: 1.5em;
                display: inline-block;
            }
            #order_too_small {
                width: 100%;
            }
            #product_list {width: 100%}
            .cart-item {width: 48%;}
            .minimalicbutton {font-size: 0.6em; max-width: 44%; padding: 8px 2%; margin: 0 1% 0 1%;}
        }
    </style>
</head>

<body>
<?php include_once('header.php'); ?>
<div id="content">
    <div class="clearfix" style="height: 20px"></div>
    <div id="left-column">
        <div class="accordion">
            <?php
            $categories = $db->getCategories();
            $products = $db->getProducts();

            foreach ($categories as $category){
                $products = $db->getProducts("`category` = $category->id","","`name`");
                if (count($products) > 0){
                    print ("<span class='blocklist-head'><span class='expander'>$category->name</span> <a href='category.php?category=".$category->id."'>Все</a> </span><ul class='blocklist'>");
                    foreach ($products as $product){
                        print "<li><a href='product.php?id=$product->id'>".$product->name."<span data-value='$product->id' class='quickAddToCart'>+</span></a></li>";
                    }
                    print ("</ul>");
                }
            }
            ?>
        </div>
        <div class="leftside_promo">
            <img src="img/drivers.png" alt="Заказать суши в Коломне - приглашаем водителей">
        </div>
    </div>
    <div id="right-column">
        <div id="main">
            <h1 style="font-size: 14px">Мой заказ</h1>

            <div id="cart-page" class="row">
                <div id="product_list"></div>
            <?php
                include_once('engine/dummies/cart_is_empty.php');
               ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

</div>
<div id="overlay"></div>
<div id="orderbox">
    <span class="heading">Оформление заказа</span>
    <div class="row" id="orderbox_body">
        <span class="label">Ваше имя</span>
        <input class="input" id="order_clientname" type="text">
        <span class="label">Ваш номер телефона</span>
        <input class="input" id="order_phone" type="tel">
        <span class="label">Ваш e-mail (необязательно)</span>
        <input class="input" id="order_email" type="email">
        <span class="label">Адрес доставки</span>
        <input class="input" id="order_address" type="text">
        <span class="label">Комментарий</span>
        <textarea class="input" id="order_comment"></textarea>
    </div>
    <div class="row" style="padding-top: 13px; margin-top: 15px; border-top: silver dotted 1px">
        <button id="createOrderButton" class="minimalicbutton">Оформить</button>
        <button id="cancelOrdering" class="minimalicbutton">Отменить</button>
    </div>
    <div class="clearfix"></div>
</div>

<?php include_once('footer.php'); ?>
</body>

</html>
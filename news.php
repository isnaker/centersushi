<?php
include('engine/config.php');
$db = new DB();

function sortl($a, $b, $sortByField = 'id') {if($a->id === $b->id)return 0;return $a->id < $b->id ? 1 : -1;}
//uasort($products, 'myCmp');  // сортировка элементов
$counter = 0;

if (isset($_GET['id'])){
    $newsId = (int) $_GET['id'];
}
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Новости. Доставка роллов в Коломне. Заказать суши в Коломне. Центр Суши</title>

    <link type="text/css" rel="stylesheet" href="css/framework.css">
    <link type="text/css" rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="css/product.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">

    <link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name='yandex-verification' content='48de752d2a7ce431' />
    <meta name="keywords" content="Центр Суши, суши, роллы, заказать, заказать роллы Коломна, роллы Коломна, суши Коломна, заказать суши Коломна, заказать роллы, заказать суши, Суши ,доставка, суши, роллы, салаты, сашими, сеты, горячее, супы, десерты, напитки, Коломна, японская кухня, блюда, суши в коломне, доставка суши в коломне, заказать суши в Коломне"/>
    <meta name="description" content="Японские блюда от «Центр Суши» с доставкой. Доставка суши и роллов на дом в Коломне."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/basic.js"></script>
    <script type="text/javascript" src="js/cart.js"></script>
    <script type="text/javascript" src="js/jquery.easing.js"></script>
    <script type="text/javascript" src="js/modernizr.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.blocklist li').hide();
            $("#left-column .expander").click(function(){
                $(this).toggleClass('expanded');
                var p = $(this).parent().next();
                if ($(this).hasClass('expanded')){
                    $(p).find('li').show(300);
                } else {
                    $(p).find('li').hide(500);
                }
            });

            $('nav a[href="news.php"]').addClass('current');
            $('#menu_controller').click(function(){
                $('#left-column .accordion').toggleClass('showed');
            });
        });
    </script>
    <style>
        h1 {margin-bottom: 1em;font-size: 2em}
        #news .item {
            float: left;
            display: inline-block;
            background: white;
            border: silver solid 1px;
            width: 43%;
            margin: 1%;
            padding: 2% 2% 2% 2%;
            transition: all 0.3s ease;
            -webkit-transition: all 0.3s ease;
        }
        #news .item:hover {
            box-shadow: 0 1px 8px rgba(0,0,0,0.26);
            cursor: default;
        }
        #news .item.expanded {width: 90%}
    </style>
</head>

<body>
<?php include_once('header.php'); ?>
<div id="content">
    <div id="left-column">
        <div class="accordion">
            <?php
            $categories = $db->getCategories("",true);
            $products = $db->getProducts();

            foreach ($categories as $category){
                $products = $db->getProducts("`category` = $category->id","","`name`");
                if (count($products) > 0){
                    print ("<span class='blocklist-head'><span class='expander'>$category->name</span> <a href='category.php?category=".$category->id."'>Все</a> </span><ul class='blocklist'>");
                    foreach ($products as $product){
                        print "<li><a href='product.php?id=$product->id'>".$product->name."<span data-value='$product->id' class='quickAddToCart'>+</span></a></li>";
                    }
                    print ("</ul>");
                }
            }
            ?>
        </div>
        <div class="leftside_promo">
            <img src="img/thursday_sale.png" alt="Каждый четверг - скидка 20%! Центр Суши - заказать суши и роллы в Коломне">
        </div>
        <div class="leftside_promo">
            <img src="img/drivers.png" alt="Заказать суши в Коломне - приглашаем водителей">
        </div>
    </div>
    <div id="right-column">
        <div id="main">
            <h1 class="text-centered">Новости "<?php print App::companyname(); ?>"</h1>
            <div id="news">
                <?php
                    //show a new
                    if ($newsId > 0){
                        $new = $db->getNews($newsId);
                        $new = $new[0];
                        print "<div><a href='/news.php'>< К другим новостям</a></div>
                                <div class='item expanded'>
                                    <h3>$new->heading</h3>
                                    <p class='description'>$new->content</p>
                                </div>";
                    }
                    else
                    {
                    //prints all news
                    $news = $db->getNews();
                        foreach ($news as $new){
                            if (strlen($new->content) > 180) {$announce = substr($new->content,0,175)."...";} else {$announce = $new->content;}
                                print "<div class='item'>
                                    <small style='float:right'>$new->date_published</small>
                                    <h3>$new->heading</h3>
                                    <p class='description'>$announce</p>
                                    <a href='/news.php?id=$new->id'>Подробнее</a>
                                </div>";
                            }
                    }
                ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>


</div>

<?php include_once('footer.php'); ?>
</body>

</html>
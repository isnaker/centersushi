<?php
include('Cart.php');

$input = $_POST;
$action = $input['action'];
$db = new DB();
$cart = new Cart();
//return print_r($input['data']['productId']);
switch ($action) {
    case "getCart":
        $cart->loadFromSession(); print(json_encode($cart));
        break;
    case "addToCart":
        $cart->addToSession($input['data']['productId'],$input['data']['productCount']);
        break;
    case "updateCart":
        $cart->updateCart($input['data']['id'],$input['data']['delta']);
        break;
    case "clearCart":
        $cart->clearCart();
        break;
    case "getOrders":
        //$orders = $db->getOrders("(`created_time` BETWEEN CURDATE()-INTERVAL 2 WEEK AND CURDATE()) OR `state` = 0 ",null,"`created_time` DESC,`state` ASC");
        $orders = $db->getOrders("`id` > 3000","","`created_time` DESC,`state` ASC");

        foreach ($orders as $order){
            $products = array();
            $products = json_decode($order->products);

            $__products = array();

            foreach ($products as &$product){
                 if ($product[1] != 0){
                     $_product = $db->getProduct($product[0]);
                     if (is_object($_product)){
                         $_product->count = (int)$product[1];
                         array_push($__products, $_product);
                     }

                 }
             }

             $order->state = (int)$order->state;
             $order->products = $__products;
        }
        print json_encode($orders);
        break;
    case "getProducts":
        $products = $db->getProducts();
        print json_encode($products);
        break;
    case "createOrder":
        $cart->loadFromSession();
        $cart->createOrder($input['data']);
        $cart->clearCart();
        break;
    case "updateOrderStatus":
        $db->updateOrderState($input['data']['id'],"state",$input['data']['status']);
        break;
    case "updateOrder":
        $db->updateOrder($input['data']);
        break;
}


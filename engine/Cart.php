<?php
session_start();

require_once('config.php');
class Cart {
    public $items = array();
    public $itemCount;

     public function addToSession($productID, $count = 1){
         //return print($productID." ".$count);
         $added =false;
         if (count($_SESSION['cart']['product']) > 0){
             // check if product already in cart - need only to inc
             for($i=0;$i<count($_SESSION['cart']['product']);$i++){
                 if ($_SESSION['cart']['product'][$i] == $productID){
                     $_SESSION['cart']['count'][$i] = (int) $count + (int)$_SESSION['cart']['count'][$i];
                     $added =true;
                 }
             }
         }
         if (!$added){
             // add new product to cart
             $_SESSION['cart']['product'][] = (int)$productID;
             $_SESSION['cart']['count'][] = (int)$count;
         }
        Logger::log('addToCart', json_encode($_SESSION['cart']));
    }

    public function loadFromSession(){
        $db = new DB();
        $count = 0;
        if (isset($_SESSION['cart'])){
            for($i=0;$i<count($_SESSION['cart']['product']);$i++){
                if ($_SESSION['cart']['product'][$i] != 0){
                    $multiplicator = 1;
                    $product = $db->getProduct( $_SESSION['cart']['product'][$i]);

                    /*TODO: sales and counts */

                    //$day = date('N');
                    //if ($day == 4 && $product->category != 8){  $multiplicator = 0.9; }

                    $this->items[$count]['product'] = $product;
                    $this->items[$count]['count'] = $_SESSION['cart']['count'][$i];
                    $this->items[$count]['multiplicator'] = $multiplicator;
                    $count++;
                }
            }
        }

    }

    public function getItems(){
        $db = new DB();
        $count = 0;
        $items = array();

        for($i=0;$i<count($_SESSION['cart']['product']);$i++){
            if ((int)$_SESSION['cart']['product'][$i] != 0){
                array_push($items, array($_SESSION['cart']['product'][$i], $_SESSION['cart']['count'][$i]))  ;
            }
        }
        return $items;
    }

    public function clearCart(){
        $this->items = array();
        unset($_SESSION['cart']);
    }

    public function updateCart($id,$delta){
        //@var $delta - amount of decreasing or increasing count of a product with @id
        // if  $delta == 0, we need to completely remove
        if ($delta == 0){
            for($i=0;$i<count($_SESSION['cart']['product']);$i++){
                if ($_SESSION['cart']['product'][$i] == $id) {
                    array_splice($_SESSION['cart']['product'],$i,1);
                    array_splice($_SESSION['cart']['count'],$i,1);
                }
            }
        } else {
            for($i=0;$i<count($_SESSION['cart']['product']);$i++){
                if ($_SESSION['cart']['product'][$i] == $id) {
                    $_SESSION['cart']['count'][$i] = $_SESSION['cart']['count'][$i] + $delta;
                    if ($_SESSION['cart']['count'][$i] == 0){
                        $_SESSION['cart']['product'][$i] = 0;
                    }
                }
            }
        }
        Logger::log('updateCart', json_encode($_SESSION['cart']));
    }

    public function removeFromCartByProductId($product_id){ // may be useful, but now not in charge
        print('prodid = '.$product_id);
        $i = 0;
        foreach ($this->items as $item){

            if ($item['product']->id == $product_id) {
                array_splice($this->items,$i,1);
                $this->itemCount--;
            }
            $i++;
        }
        print('<hr>');
    }
    // now this function wants to get human counting of a position to remove (0 (real) = 1 (human))
    public  function removeFromCart($position){
        array_splice($this->items,$position-1,1);
        $this->itemCount--;
    }

    public function changeProductCount($position,$delta){
        $this->items[$position]['count'] += $delta;
    }

    public function getCart(){
        return json_encode($this->items);
    }

    public function getPrice(){
        $db = new DB();
        $price = 0;
        foreach ($this->items as $item){
            $product = $db->getProduct($item);
            $price += $product->price;
        }
        return $price;
    }

    public function count(){
        return $this->itemCount;
    }

    static function createInstance($object){
        //unset($_SESSION['cart']);
        $cart = new Cart();
        print('creating instance');
        $cart = unserialize($object);
        print_r($cart);
        //foreach ($object as $obj){
       ///     $cart->addToCart($object);
        //}
        return $cart;
    }

    function createOrder($data){
        $db = new DB();
        $data['products'] = json_encode($this->getItems());
        print $db->createOrder($data);
    }

    function __construct(){

   }
}
/*unset($_SESSION['cart']);

$cart = new Cart();
$cart->addToSession(37,1);
$cart->addToSession(38,2);
$cart->addToSession(39,2);

$data = array();
$data['name'] = "name";
$data['price'] = "44545";

print_r ($cart->createOrder($data));*/






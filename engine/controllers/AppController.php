<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 17.01.2017
 * Time: 23:08
 */

$root = $_SERVER['DOCUMENT_ROOT'];
include_once($root.'/engine/classes/Options.php');
include_once($root."/engine/classes/DB.php");
include_once($root.'/engine/Product.php');
include_once($root.'/engine/Roll.php');
include_once($root.'/engine/Pizza.php');
include_once($root.'/engine/User.php');
include_once($root.'/engine/Logger.php');
include_once($root."/engine/classes/App.php");

const TEMPLATE_TWO_COLUMNS = "/engine/parts/template.php";
const TEMPLATE_ONE_COLUMN = "/engine/parts/template_one_row.php";

$app = new App();
$db = new DB();

$page = new stdClass();
$page->title = "Центр Суши";
$page->description = "";
$page->keywords = "";

$page->default_template = TEMPLATE_ONE_COLUMN;
$page->template = $page->default_template;

$includable = array();

$categories_list = $db->getCategories();

$categories_menu = $db->get('*','categories', "WHERE `id` IN (1,2,5,8,10,12,17)");

if (isset($_GET['path'])){
    $path = $_GET['path'];

    switch ($path) {
        case "home":
            $content_top = "/engine/parts/slider.php";
            $prod = $db->getPopular(8,12);
            $sushi = $db->getProductGroup("index_sushi");
            $page->title = "Центр Суши - заказать роллы и пиццу с доставкой в Коломне";
            $includable = array("/engine/pages/home.php");
            break;
        case "menu":
            $categories = $db->getCategories();
            $includable = array("/engine/pages/menu.php");
            break;
        case "category":
            $category = $db->get('*',"categories","WHERE `slug` = '".$_GET['action']."' LIMIT 1");
            $products = $db->getProductsByCategory($category->id);

            $page->title = "Заказать $category->name с доставкой в Коломне. Центр Суши.";
            $includable = array("/engine/pages/category.php");
            break;
        case "product":
            $product = $db->getProduct($_GET['action']);
            $popular = $db->getPopular(3, $product->category);

            //$category = $db->get("*", "categories", "WHERE `id` = '$product->category'")->name;

            $page->title = "Заказать $product->name с доставкой в Коломне. Центр Суши.";
            $includable = array("/engine/pages/product.php");
            break;
        case "cart":
            break;
        case "contacts":
            $includable = array("/engine/pages/404.php");
            break;
        case "actions":
            break;
        default:
            $includable = array("/engine/pages/404.php");
            $page->template = TEMPLATE_ONE_COLUMN;
            break;
    }
}

include_once($root.$page->template);


?>
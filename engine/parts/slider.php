<div class="container">
    <div id="slider">
        <div class="flexslider">
            <ul class="slides">
                <li>
                    <img src="/img/slides/monday.jpg" class="img-responsive" />
                </li>
                <li>
                    <img src="/img/slides/diner.jpg" class="img-responsive" />
                </li>
                <li>
                    <img src="/img/slides/birthday.jpg" class="img-responsive" />
                </li>
                <li>
                    <img src="/img/slides/1000.jpg" class="img-responsive" />
                </li>
                <li>
                    <img src="/img/slides/1500.jpg" class="img-responsive" />
                </li>
                <li>
                    <img src="/img/slides/2000.jpg" class="img-responsive" />
                </li>
            </ul>
        </div>
    </div>
</div>

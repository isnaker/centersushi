<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                    <a href="http://www.snapix.ru" target="_blank"><img src="/img/snapix.png" alt="Snapix - студия IT-решений для бизнеса"> </a>
            </div>
            <div class="col-md-4 text-center">
                <img src="/img/logo.png" alt="Центр суши">
            </div>
            <div class="col-md-4">
                <p>ООО "Центр Суши"</p>
                <p>ИНН 455080516187</p>
                <p><strong>Наш адрес:</strong>
                    <?php  $app->address();  ?></p>
                <p class="phones"><strong>Наш телефон:</strong>
                    <strong><a href="tel:/<?php  $app->phone(); ?>"><?php  $app->phone(); ?></a></strong>
                </p>
            </div>
        </div>
    </div>
</footer>
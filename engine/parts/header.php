<header>
    <div id="header-container">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <a id="logo" href="/"> <img alt="Центр суши" src="/img/logo.png" class="img-responsive"> </a>
                </div>
                <div class="col-md-3">
                    <div class="header-block" id="header-promo">
                        <h2><a href="tel:<?php $app->phone(); ?>"> <?php $app->phone(); ?></a></h2>
                        <h4 id="header-annotation">Доставка суши и пиццы в Коломне</h4>
                        <h4 class="small">Минимальный заказ - 450 руб.</h4>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="header-block header-contacts">
                        <p class="h3">Время работы:</p>
                         <div class="row">
                            <div class="col-md-4">
                                <p class="h4">11:00 - 00:00</p>
                                <p class="h6">Понедельник - Четверг</p>
                            </div>

                            <div class="col-md-4">
                                <p class="h4">11:00 - 02:00</p>
                                <p class="h6">Пятница и суббота</p>
                            </div>

                            <div class="col-md-4">
                                <p class="h4">11:00 - 00:00</p>
                                <p class="h6">Воскресенье</p>
                            </div>


                        </div>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="header-block" id="cart">
                        <p class="text-center h5">
                            <a href="/new/cart">
                                <i class="fa fa-shopping-cart"></i> Корзина
                                <span id="minicart_productCount">(<span data-value="cart-count">
                                        <i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i>
                                    </span>)
                                </span>
                            </a>
                        </p>

                        <div id="weAccept" class="text-center">
                            <p class="nopadding"> Мы принимаем</p>
                            <img src="/img/visa.png" alt="Мы принимаем Visa Mastercard">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="nav">
    <div class="container">
        <ul class="nav nav-pills">
            <li role="presentation"

                <?php if ($_SERVER['REQUEST_URI'] == "/new") { ?> class="active" <?php }  ?>
            >
                <a href="/new">Главная</a>
            </li>

            <?php foreach($categories_menu as $item){ ?>
                <li role="presentation"
                    <?php if ("/new/category/$item->slug" == $_SERVER['REQUEST_URI']) { ?> class="active" <?php }  ?>
                >
                    <a href="/new/category/<?= $item->slug ?>"><?= $item->name ?></a>
                </li>
            <?php } ?>
            <li role="presentation"

                <?php if ($_SERVER['REQUEST_URI'] == "/new/actions") { ?> class="active" <?php }  ?>
            >
                <a href="/new/actions">Акции</a>
            </li>
            <li role="presentation">
                <a href="/new/contacts">Контакты</a>
            </li>

        </ul>
    </div>
</div>



<span id="menu_controller" class="visible-xs"><img src="/img/icon-menu.png" alt="показать меню"> Показать меню</span>

<div id="mobile_nav_container">
    <div class="content">

        <div class="toggler"><i class="fa fa-arrow-circle-left fa-2x color-white"></i></div>
        <div class="float_cart"><a href="/cart.php" class="color-white"> <i class="fa fa-shopping-bag"></i> <span data-value="cart-count"></span> </a></div>
        <img src="/img/logo.png" alt="Центр Суши логотип">
        <div class="menu items">
            <?php
            foreach ($categories as $category){
                $products = $db->getProducts("`category` = $category->id","","`name`",false);
                if (count($products) > 0){ ?>
                    <a class="item color-white" href='category.php?category="<?= $category->id ?>"'><?= $category->name ?></a>
                <?php }
            }
            ?>
        </div>

        <div class="footer color-white">
            <div><small>Навигация</small></div>

            <div class="mt-lg nav">
                <a href="/">Главная</a>
                <a href="/menu.php">Меню</a>
                <a href="/actions.php">Акции</a>
                <a href="/contacts.php">Контакты</a>
            </div>

            <div class="mt-xlg">Центр Суши, <?= date("Y") ?></div>
        </div>

    </div>
</div>
<div id="mobile_nav_toggle" class="visible-xs"><i class="fa fa-bars"></i> </div>
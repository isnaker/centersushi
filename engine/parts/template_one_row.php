<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $page->title ?></title>

    <meta name='yandex-verification' content='48de752d2a7ce431' />
    <meta name="keywords" content="<?= $page->keywords ?>"/>
    <meta name="description" content="<?= $page->description ?>"/>

    <link href="/css/min/bootstrap.min.css" rel="stylesheet">

    <link href="/css/flexslider.css" type="text/css" rel="stylesheet">
    <link href="/css/theme.css" type="text/css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" type="text/css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Play:700" rel="stylesheet">
    <?php if (isset($add_css)) {
        foreach ($add_css as $item){ ?>
            <link href="<?php print $item; ?>" type="text/css" rel="stylesheet">
        <?php }
    } ?>
</head>
<body>
<?php include_once($_SERVER['DOCUMENT_ROOT']."/engine/parts/header.php"); ?>

<div id="content">
    <?php  if (isset($content_top)) { include_once($root.$content_top); } ?>
    <div class="container">
        <div class="row">
              <div class="col-md-12">
                <?php if (isset($includable)){
                    foreach($includable as $item)
                    {
                        include_once($root.$item);
                    }
                } ?>
            </div>
        </div>
    </div>
    <?php if (isset($content_bottom)) { include_once($root.$content_bottom); } ?>
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT']."/engine/parts/footer.php"); ?>

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>


<script type="text/javascript" src="/js/basic.js"></script>
<script type="text/javascript" src="/js/cart.js"></script>
<script type="text/javascript" src="/js/jquery.easing.js"></script>
<script type="text/javascript" src="/js/modernizr.js"></script>
<script type="text/javascript" src="/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="/js/basic.js"></script>
<script type="text/javascript" src="/js/scripts.js"></script>

<?php if (isset($add_js)) {
    foreach ($add_js as $item){ ?>
        <script src="<?php print $item; ?>" type="text/javascript"></script>
    <?php }
} ?>

</body>
</html>
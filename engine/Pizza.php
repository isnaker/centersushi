<?php

class Pizza extends Product {

    public function __construct($data){
        $db = new DB();
        $this->type = "roll";
        $this->id = (int) $data->id;
        $this->name = $data->name;
        $this->price =(int) $data->price;
        $this->weight =(int) $data->weight;
        $this->description = $data->description;
        $this->category =(int) $data->category;
        $this->requested =(int) $data->requested;
        $this->spicy =(int) $data->spicy;
        $this->isVisible =(int) $data->isVisible;
        $this->count = 0;
        // get ingredients
        if ($data->ingredients != ""){
            $ingredients = $db->getIngredients($data->ingredients);
        }

        if (isset($ingredients) && count($ingredients) > 0){
            foreach ($ingredients as $ingredient){
                array_push($this->ingredients, $ingredient);
            }
        }

        // get image
        $image = $db->getProductImage($this->id);
        if ($image) {
            $image = ''.$image[0]->image_url;
            if (!file_exists($image)){
                //*TODO:*/
                //Fix missing images replacement
                //$image = 'http:/center_sushi/img/products/product-example.jpg';
            }
            $this->image = $image;
        }
    }

    public function incrementRequested(){
        $db = new DB();
        $db->incrementRequested($this->id);
    }

    public function get(){
        return $this;
    }

    public function write(){
        $ingredients = "";
        foreach ($this->ingredients as $ingredient){
            $ingredients.="<span class='product-ingredient'>$ingredient->name</span>";
        }

        $spicyText = "";
        if ($this->spicy != 0){$spicyText = "<img class='product-addictional-icon' src='img/pepper-minimalistic.png' alt='Острый!' >";}

        $output = "<a data-id='$this->id' href='product.php?id=$this->id' class='product'>

                        <div class='product-image'>
                            <img src='/img/products/120/$this->image' alt='$this->name'>
                        </div>
                        <div class='product-details'>
                            <h3 class='product-name'>$this->name $spicyText</h3>
                            <p class='product-description'><span>Вес: $this->weight г.</span></p>
                            <h3 class='product-price'>
                                <span class='product-price-value'>$this->price</span> <span class='rub'>c</span>

                            </h3>
                                 <span title='Добавить в корзину' data-value='$this->id' class='quickAddToCart'>+ <span>К заказу</span></span>
                            <p class='product-ingredients-list'>".$ingredients."</p>
                        </div>
                    </a>";
        print $output;
    }

    function writeModal(){
        $ingredients = "";
        foreach ($this->ingredients as $ingredient){
            $ingredients.="<span class='product-ingredient'>$ingredient->name</span>";
        }
        $requestedText = 'раз';

        $spicyText = "";
        if ($this->spicy != 0){$spicyText = "<img class='product-addictional-icon' src='img/pepper-minimalistic.png' title='Острый!' alt='Острый!' >";}

        // RADIUS
        if ($this->category == "12"){
            $radius = 32;
        }

        if ($this->category == "13"){
            $radius = 42;
        }

        $output = "<div class='product product-preview'>
                        <div class='product-image'>
                            <img src='/img/products/800/$this->image' alt='".$this->name."'>
                        </div>
                        $spicyText
                        <div class='product-details'>
                            <h1 class='product-name'>$this->name</h1>
                            <p class='product-description'>
                                <span><img src='img/weight-icon.png' title='Вес' alt='Вес' id='product-weight'> $this->weight г.</span>

                                <span><img src='img/eye-icon.png' title='Количество просмотров' alt='Просмотрено' id='product-viewed'>$this->requested </span></p>
                                <small id='annotation'>* Указан вес сырого продукта</small>
                            <h3 class='product-price'>
                                <span class='product-price-value'>$this->price</span> <span class='rub'>c</span>
                            </h3>
                            <h4 class='clearfix' style='margin:5px 0'>Диаметр: $radius см</h4>
                            <p class='product-ingredients-list'>".$ingredients."</p>".
                            $this->makeDescriptionIslands()."
                            <div class='clearfix'></div>

                            <div class='clearfix'></div>
                            <button data-value='$this->id' id='addToCartButton' class='minimalicbutton'>Добавить к заказу</button>
                            <div id='addToCartButton_response'>Добавлено в корзину</div>

                        </div>
                    </div>";
        print $output;
        // TODO:
        // Increment "requested times" counter
        $this->incrementRequested();
    }
}

<div class="col-md-12">
    <p class="h2 strong mb-lg"><?= $category->name ?></p>
</div>


<div class="products-list">
    <?php foreach ($products as $product) { ?>
        <div class="col-md-3">
            <div class="product-item item">
                <p class="h4 text-center"><a href="/new/product/<?= $product->id  ?>"><?= $product->name ?></a></p>

                <div class="image">
                    <a href="/new/category/<?= $db->get("*", "categories", "WHERE `id` = '$product->category'")->slug  ?>/<?= $product->id  ?>">
                        <img src="/img/products/300/<?= $product->image ?>" alt="" class="img-responsive">
                    </a>

                    <button
                        data-value='<?= $product->id ?>'
                        class='btn btn-default btn-theme mt-md quickAddToCart'>+
                    </button>
                </div>
                <div class="row text-center">
                    <div class="col-md-6">
                        <p class="h4 strong"><?= $product->price ?> <i class="fa fa-rouble"></i></p>
                    </div>
                    <div class="col-md-6">
                        <p class="h6 mt-lg borderLeft"><i class="fa fa-balance-scale"></i> <?= $product->weight ?> гр. </p>
                    </div>
                </div>
                <div class='response hidden'>Добавлено в корзину</div>

            </div>
        </div>
    <?php } ?>
</div>

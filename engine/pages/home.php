
<h1 class="h3 text-center">Заказать пиццу и роллы в Коломне. Центр Суши.</h1>

<div class="row">

    <div class="col-md-6">
        <a class="promo_link" href="/new/category/rolls" style="background-image: url('/img/promo_link/rolls.jpg')">Роллы</a>
    </div>
    <div class="col-md-6">
        <a class="promo_link" href="/new/category/pizza_42" style="background-image: url('/img/promo_link/pizza.jpg')">Пицца</a>
    </div>

</div>

        <div id="groups">
            <p class="h3 mt-none">Популярная пицца <img src="/img/new_item_icon.png" alt="новое"></p>
            <div class="row">

                <?php  foreach ($prod as $product){ ?>
                    <div class="col-md-3">
                        <div class="product-item item">
                            <p class="h4 text-center"><a href="/new/product/<?= $product->id  ?>"><?= $product->name ?></a></p>

                            <div class="image">
                                <a href="/new/product/<?= $product->id  ?>">
                                    <img src="/img/products/300/<?= $product->image ?>" alt="" class="img-responsive">
                                </a>

                                <button
                                    data-value='<?= $product->id ?>'
                                    class='btn btn-default btn-theme mt-md quickAddToCart'>Добавить к заказу
                                </button>
                            </div>
                            <div class="row text-center">
                                <div class="col-md-6">
                                    <p class="h4 strong"><?= $product->price ?> <i class="fa fa-rouble"></i></p>
                                </div>
                                <div class="col-md-6">
                                    <p class="h6 mt-lg borderLeft"><i class="fa fa-balance-scale"></i> <?= $product->weight ?> гр. </p>
                                </div>
                            </div>
                            <div class='response hidden'>Добавлено в корзину</div>

                        </div>
                    </div>
                <?php }   ?>
            </div>

            <div class="row">
                <p class="h3">Самые популярные</p>
                <?php
                $i = 0;
                $prod = $db->getPopular(8);
                foreach ($prod as $product){ ?>
                    <div class="col-md-3">
                        <div class="product-item item">
                            <p class="h4"><?= $product->name ?></p>

                            <div class="image">
                                <a href="/product/<?= $product->id  ?>">
                                    <img src="/img/products/300/<?= $product->image ?>" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="h3 mt-md"><?= $product->price ?> <i class="fa fa-rouble"></i></p>
                                    <p class="h6 mt-none"><i class="fa fa-balance-scale"></i> <?= $product->weight ?> гр. </p>
                                </div>
                                <div class="col-md-6">
                                    <button data-value='<?= $product->id ?>'
                                            class='btn btn-default btn-theme mt-md quickAddToCart'>Добавить к заказу</button>
                                </div>
                            </div>
                            <div class='response hidden'>Добавлено в корзину</div>

                        </div>
                    </div>
                <?php }   ?>
            </div>

            <div class="group-container">
                <p class="h3">Подборка суши</p>
                <?php

                foreach ($sushi as $product){ ?>
                    <div class="col-md-4">
                        <div class="product-item item">
                            <p class="h4"><?= $product->name ?></p>

                            <div class="image">
                                <a href="/product/<?= $product->id  ?>">
                                    <img src="/img/products/300/<?= $product->image ?>" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="h3 mt-md"><?= $product->price ?> <i class="fa fa-rouble"></i></p>
                                    <p class="h6 mt-none"><i class="fa fa-balance-scale"></i> <?= $product->weight ?> гр. </p>
                                </div>
                                <div class="col-md-6">
                                    <button data-value='<?= $product->id ?>'
                                            class='btn btn-default btn-theme mt-md quickAddToCart'>Добавить к заказу</button>
                                </div>
                            </div>
                            <div class='response hidden'>Добавлено в корзину</div>

                        </div>
                    </div>
               <?php  } ?>
                <div class="clearfix"></div>
            </div>
        </div>




<div class="row">
    <div class="col-md-12">
        <div class='breadcrumbs'>
            <a href='/'>Центр суши</a> >
            <a href='/new/menu'>Меню</a> >
            <a href="/new/category/<?= $product->category ?>"><?php print $db->getCategoryNameById($product->category); ?></a> >
            <span><?php print $product->name; ?></span>
        </div>
    </div>
</div>


<div class="product-card row">
        <div  class="col-md-6">
            <img class="img-responsive img-thumbnail mt-md" src="/img/products/800/<?= $product->image ?>" alt="">
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <p class="h2 mt-md"><?= $product->name ?></p>

                    <span><i class="fa fa-balance-scale"></i> <?= $product->weight ?> г.</span>
                    <span><i class="fa fa-eye"></i> <?= $product->requested ?></span>
                </div>
                <div class="col-md-6">
                    <h3 class='product-price'>
                        Цена: <span class='product-price-value'><?= $product->price ?> <i class="fa fa-rouble"></i></span>
                    </h3>
                    <div><button data-value='<?= $product->id ?>' id='addToCartButton' class='btn btn-theme'>Добавить к заказу</button></div>
                    <div id='addToCartButton_response' class="hidden">Добавлено в корзину</div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">


                    <p class='product-ingredients-list'>
                        <span class="strong">Состав:</span>
                        <?php foreach ($product->ingredients as $ingredient) { ?>
                            <span><?= $ingredient->name ?></span>
                        <?php } ?>
                    </p>
                    <?= $product->description ?>
                </div>
            </div>



        </div>
</div>

<div class="products-list row">
    <p class="h4 strong mt-xlg">Самые популярные</p>
    <?php foreach ($popular as $product) { ?>
        <div class="col-md-3">
            <div class="product-item item">
                <p class="h4"><?= $product->name ?></p>

                <div class="image">
                    <a href="/new/product/<?= $product->id  ?>">
                        <img src="/img/products/300/<?= $product->image ?>" alt="" class="img-responsive">
                    </a>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p class="h3 mt-md"><?= $product->price ?> <i class="fa fa-rouble"></i></p>
                        <p class="h6 mt-none"><i class="fa fa-balance-scale"></i> <?= $product->weight ?> гр. </p>
                    </div>
                    <div class="col-md-6">
                        <button data-value='<?= $product->id ?>'
                                class='btn btn-default btn-theme mt-md quickAddToCart'>Добавить к заказу</button>
                    </div>
                </div>
                <div class='response hidden'>Добавлено в корзину</div>

            </div>
        </div>
    <?php } ?>
</div>




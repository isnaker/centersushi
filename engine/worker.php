<?php
    include_once('config.php');
    $db = new DB();

    if ($_POST){


    if ($_POST['action'] == "addProduct"){
        $params = $_POST;
        $db->addProduct($params);
    }
        if ($_POST['action'] == "addPizza"){
            /* first params */
            $first = array();
            $first['name'] = $_POST['name'];
            $first['price'] = $_POST['price33'];
            $first['weight'] = $_POST['weight33'];
            $first['image'] = $_POST['image'];
            $first['ingredients'] =  $_POST['ingredients'];
            $first['details'] = $_POST['details'];
            $first['category'] = "12";
            $db->addProduct($first);

            $id = $db->getLastId();
            $image = $db->getProductImage($id);

            /* first params */
            $second = array();
            $second['name'] = $_POST['name'];
            $second['price'] = $_POST['price42'];
            $second['weight'] = $_POST['weight42'];
            $second['image'] = "";
            $second['ingredients'] = $_POST['ingredients'];
            $second['details'] = $_POST['details'];
            $second['category'] = "13";
            $db->addProduct($second);

            $last = $db->getLastId();
            $db->updateProductImage($last,$image);

        }

    }

    if ($_GET)
    if ($_GET['action']=='deleteProduct'){
          $id = $_GET['id'];
          if ($db->deleteProduct($id)){
                header("worker.php");
          };
    }
?>
<html>
<head>
    <title>Центр Суши - Управление</title>
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="/css/framework.css">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <style type="text/css">
        * {
            font-family: 'Helvetica', sans-serif;
        }
        body {
            min-width: 1200px;
        }
        #panel_products {
            display: inline-block;
            min-width: 300px;
            width: 20%;
            min-height: 600px;
            float: left;
            margin-right: 2%;
        }

        #products-list {
            height: 800px;
            overflow: scroll;
        }

        #ingredientsList {
            display: inline-block;
            width: 100%;
            font-size: 11px;
            clear: both;
            float: left;
        }
        .ingredient-searchable{
            display: inline-block;
            line-height: 1.5;
            width: 25%;
        }
    </style>
    <script type="text/javascript" src="../js/jquery-1.11.0.min.js"></script>
</head>
<body style="margin: 25px">

<h1>betaAdmin for CenterSushi</h1>
<hr>

<div id="panel_products" class="panel">
    <h2>Продукты</h2>
    <div id="products-list">
    <table style="width: 100%">
    <?php
        $products = $db->getProducts("",""," `id` DESC",true);

        foreach ($products as $product){
            print "<tr><td>".$product->get()->id."</td><td>".$product->get()->name."</td><td>".$product->get()->price."</td><td><a style='font-size:11px' href='worker.php?action=deleteProduct&id=".$product->get()->id."'>Удалить</a></td></tr>";
        }
    ?>
    </table>
    </div>
</div>

<div style="float: left; width: 72%">
<div id="panel_addProduct" class="panel box33p">
    <h2>Добавление нового продукта</h2>

    <form enctype="multipart/form-data" method="post" action="worker.php" >
        <input hidden="hidden" name="action" value="addProduct">
        <input name="name" placeholder="name"><br>
        <input width="10" size="10" name="price" placeholder="price"><br>
        <input width="10" size="10" name="weight" placeholder="weight"><br>

        <select name="category">

            <?php
                $cat = $db->getCategories("",true);
                foreach ($cat as $category){
                    print ("<option value=".$category->id.">$category->name</option>");
                }
            ?>

        </select>
        <br>
        <textarea name="details" placeholder="details"></textarea><br>


        <select style="display: none" name="rating">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select>
        <br>
        <input id="ingredients" name="ingredients" placeholder="ingredients"><br>
        <input name="radius" placeholder="radius"><br><br>
        Изображение товара<br><input type="file" name="image"><br><br>

        <br><br><br><br><br><br><br>
       <hr>
        <input type="submit" value="Добавить продукт">
    </form>
</div>

<div id="panel_addPizza" class="panel box33p">
    <h2>Добавить новую пиццу</h2>
    <form enctype="multipart/form-data" method="post" action="worker.php" >
        <input hidden="hidden" name="action" value="addPizza">
        <h3>Название</h3><br><br>
        <input name="name" placeholder="name"><br><br>
        <input width="10" size="10" name="weight33" placeholder="weight33"><br>
        <input width="10" size="10" name="price33" placeholder="price33"><br><br>

        <input width="10" size="10" name="weight42" placeholder="weight42"><br>
        <input width="10" size="10" name="price42" placeholder="price42"><br><br>

        <textarea name="details" placeholder="details"></textarea><br>

        <input id="ingredients2" name="ingredients" placeholder="ingredients"><br><br>
        Изображение пиццы<br><input type="file" name="image"><br><br>
        <hr>
        <input type="submit" value="Добавить пиццу">
    </form>
</div>

<div id="ingredientsList">
    <h2>Ингредиенты</h2>
    <?php
    $ingredients = $db->getIngredientsList();
    foreach ($ingredients as $ingredient){
        print ("<div class='ingredient-searchable'><input class='ingredient' type='checkbox' name='ingredient".$ingredient->id."' value='".$ingredient->id."'>".$ingredient->name."</div>");
    }
    ?>
</div>
    <div class="clearfix"></div>
</div>

</body>
<script type="text/javascript">
    var ingredients = [];
    $(document).ready(function(){
        $('.ingredient').click(function(){
            $(this).toggleClass('selected');
            composeIngredients();
        })
    });

    function composeIngredients(){
        var data = $('.selected');
        var ingredientsString = "";
        for (var i=0; i<data.length; i++){
            ingredientsString+=data[i].value;
            (i != data.length-1) ? ingredientsString+=',' : '' ;
        }
        $('#ingredients').val(ingredientsString);
        $('#ingredients2').val(ingredientsString);
    }
</script>

</html>
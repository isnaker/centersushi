<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 17.01.2017
 * Time: 23:32
 */


class DB{
    private $db_name;
    private $db_user;
    private $db_password;
    private $db_host;

    private $db_status = false;
    protected $connector = null;

    public function __construct(){ $options = new Options();  $this->connect($options);  }

    public function connect($options){
        try {
            $this->connector = new PDO("mysql:host=$options->db_host;dbname=$options->db_name",$options->db_user,$options->db_password);
        } catch (PDOException $e){ print $e->getMessage(); }
        $this->connector->query('SET NAMES `UTF8`');
    }

    public function disconnect(){
        $this->db_status = false;
    }

    public function status(){ return $this->db_status;   }


    public function get($what = "*", $table = "", $params = ""){

        $query = "SELECT $what FROM `$table` $params";
        //var_dump($query);
        $res = $this->connector->query($query);

        try {
            if (is_a($res, 'object') || is_a($res, 'PDOStatement')) {
                $res->setFetchMode(PDO::FETCH_OBJ);
                $res = $res->fetchAll();
                if (count($res) == 1){$res = $res[0];}
            } else {
                $res = "Ошибка подключения к таблице $table";
            }
        } catch (Exception $e) {
            $res = "err".$e->getMessage();
        }
        return $res;
    }

    public function add($what = array(), $values = array(), $table = "", $params = "", $showquery = 0){
        $str_columns = "";
        $str_values = "";

        if (count($what) != count($values)) {
            throw new Exception("Data not similar!");
        }

        for ($i = 0; $i < count($what); $i++){
            $str_columns.= " `$what[$i]`";
            $str_values.= "'".$values[$i]."'";
            ($i < count($what)-1) ? $str_columns.= "," : "";
            ($i < count($what)-1) ? $str_values.= "," : "";
        }

        $response['query'] = "INSERT INTO `$table` ($str_columns) VALUES ($str_values) $params";
        $response['params'] = json_encode($values);

        try {
            $query = $this->connector->prepare("INSERT INTO `$table` ($str_columns) VALUES ($str_values) $params");
            $result = $query->execute($values);

            if (($result == true)) {
                $response['response_text'] = "OK";
            } else {
                $response['response_text'] = "Ошибка подключения к таблице $table";
            }
        } catch (Exception $e) {
            $response['response_text'] = "err".$e->getMessage();
            //print $e->getMessage();
        }

        return $response;
    }

    public function set($what = array(),$values = array(), $table = "", $params = ""){
        $str = "";

        if (count($what) != count($values)) {
            throw new Exception("Data not similar!");
        }

        for ($i = 0; $i < count($what); $i++){
            $str.= " `$what[$i]`='$values[$i]'";
            ($i < count($what)-1) ? $str.= "," : "";
        }

        $query = "UPDATE `$table` SET $str $params";

        $res = $this->connector->query($query);

        try {
            if (is_a($res, 'object') || is_a($res, 'PDOStatement')) {
                $res->setFetchMode(PDO::FETCH_OBJ);
                $res = $res->fetchAll();
            } else {
                $res = "Ошибка подключения к таблице $table";
            }
        } catch (Exception $e) {
            $res = "err".$e->getMessage();
        }
        return $res;
    }

    public function remove($table, $params){
        $query = "DELETE FROM `$table` $params";
        $result = $this->connector->exec($query);
        return $result;
    }

    public function removeById($table, $id){
        $id = (int) $id;
        $query = "DELETE FROM `$table` WHERE `id` =$id";
        $result = $this->connector->query($query);
        return $result;
    }

    public function getLastId(){
        return $this->connector->lastInsertId();
    }

    /* parsing and making some queries */
    private function request($request){
        try {
            $result = $this->connector->query($request);
        } catch (PDOException $e){
            print $e->getMessage();
            return 0;
        }

        $result->setFetchMode(PDO::FETCH_OBJ);
        $result = $result->fetchAll();
        return $result;
    }

    public function exec($query){
        try {
            $result = $this->connector->query($query);
        } catch (PDOException $e){
            print $e->getMessage();
            return 0;
        }
    }

    public function getProductImage($id){
        $query = "SELECT `image_url` FROM `products_images` WHERE `product_id`='$id'";
        $result = $this->request($query);
        return $result;
    }

    public function updateProductImage($productId, $newImage){
        $query = "UPDATE `products_images` SET `image_url`='$newImage' WHERE `id`='".$productId."'";
        try {
            $this->request($query);
        } catch (Exception $e){
            print "Ошибка при обновлении изображения продукта";
        }
    }

    public function removeProductImage($id){
        /*TODO:
            remove DB field and an image file
        */
    }

    public function getProducts($param = "", $limit = "", $order="", $adminMode = false){

        if ($adminMode == true) { $query = "SELECT * FROM `products`"; } else { $query = "SELECT * FROM `products` WHERE `isVisible`='1'"; };

        if ($adminMode && $param){$query.=" WHERE ".$param;}
        if (!$adminMode && $param){ $query.=" AND ".$param;}
        if ($order){ $query.=" ORDER BY ".$order;}
        if ($limit){ $query.=" LIMIT ".$limit;}

        $result = $this->request($query);
        $products = array();

        foreach ($result as $r){
            $p = Product::createInstance($r);
            array_push($products, $p);
        }
        return $products;
    }


    public function getProduct($id){
        $result = $this->request("SELECT * FROM `products` WHERE `isVisible`='1' AND `id`='".$id."'");
        try {
            if ($result){
                $p = Product::createInstance($result[0]);
            } else return 'no such object';

        } catch (Exception $e){
            return 'no such object';
        }
        return $p;
    }

    public function getProductsByCategory($category = ""){
        if ($category == ""){
            return $this->getProducts("");
        } else return $this->getProducts("`category` in (".$category.")");
    }

    public function getProductGroup($group = ""){
        $products = $this->request("SELECT `products` FROM `groups` WHERE `name`='".$group."'");
        $result = $this->getProducts("`id` IN (".$products[0]->products.")");
        return $result;
    }

    public function getPopular($limit = 3, $fromCategory = ""){
        $fromCategory_ = "";
        if ($fromCategory != ""){$fromCategory_ = "`category`='$fromCategory'";}
        $products = $this->getProducts($fromCategory_,$limit,"requested DESC");
        return $products;
    }

    public function addProduct($params){
        $query = $this->connector->prepare("INSERT INTO `products` (`name`,`price`,`description`,`category`,`ingredients`,`weight`,`rating`,`isVisible`) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
        $result = $query->execute(array(
            $params['name'],$params['price'],$params['details'],$params['category'],$params['ingredients'], $params['weight'], 0, $params['isVisible']
        ));
        if (!$result){ print("Произошла ошибка при добавлении товара");  }

        /*TODO:
        *    Highlight next block into separate "attachImageToProduct()" method
        *    of "imageWorker" class to have an ability to
        *    simply attach and replace image when changing products
        */
        if ($_FILES){
            //place image to /img/products
            $this->uploadFiles( $this->connector->lastInsertId());
        }
    }

    public function updateProduct($params){

        $query = $this->connector->prepare("UPDATE `products` SET `name`=?,`price`=?,`description`=?,`category`=?,`ingredients`=?,`weight`=?,`rating`=?,`isVisible`=? WHERE `id`=?");
        $result = $query->execute(array(
            $params['name'],$params['price'],$params['details'],$params['category'],$params['ingredients'], $params['weight'], $params['rating'], $params['isVisible'], $params['id']
        ));
        if (!($result)){ print("Произошла ошибка при обновлении товара"); }
        // update image if needed
        if ($_FILES){
            //place image to /img/products
            $this->uploadFiles($params['id']);
        }
    }

    function uploadFiles($id){
        $response = [];
        $file = $_FILES['image'];
        $target_path = $_SERVER['DOCUMENT_ROOT']."/img/products";
        $ext = explode('.', basename( $file['name']));
        $filename = md5(uniqid()) . "." . $ext[count($ext)-1];

        if (!is_dir($target_path."/original")){ mkdir($target_path."/original/"); }

        $target_path_to_original = $target_path."/original/".$filename;

        if(move_uploaded_file($_FILES['image']['tmp_name'], $target_path_to_original)) {
            $simpleimage = new SimpleImage($target_path_to_original);

            // resize to big
            if (!is_dir($target_path."/800")){ mkdir($target_path."/800");}
            $simpleimage->resizeToWidth(800);
            $simpleimage->save($target_path."/800/".$filename);
            // resize to medium
            if (!is_dir($target_path."/300")){ mkdir($target_path."/300");}
            $simpleimage->resizeToWidth(300);
            $simpleimage->save($target_path."/300/".$filename);
            // resize to small
            if (!is_dir($target_path."/120")){ mkdir($target_path."/120");}
            $simpleimage->resizeToWidth(120);
            $simpleimage->save($target_path."/120/".$filename);

            //remove previous if existed
            // append to database
            $this->remove("products_images","WHERE `product_id`='".$id."'");
            $this->add(array('image_url','product_id'),array($filename, $id),'products_images');
            $response['result'] = "OK";
        } else{
            $response['result'] = "Error";
        }
        return json_encode($response);
    }



    public function deleteProduct($id){
        $query = "DELETE FROM `products` WHERE `id`='".$id."'";
        try {
            $this->request($query);

            /*TODO:
             * Highlight next block as part of "imageWorker" class
             * Unlinking image from product and deleting associated file
             * */
            $query = "SELECT * FROM `products_images` WHERE `product_id`='".$id."'";
            $result = $this->request($query);

            if ($result)
                try {
                    $img= $result[0]->image_url;
                    if (file_exists($img)){ unlink($img); }
                } catch (Exception $E){}

            $query = "DELETE FROM `products_images` WHERE `product_id`='".$id."'";
            $this->request($query);
        } catch (Exception $E){return false;}
        return true;
    }

    public function incrementRequested($id){
        $result = $this->request("SELECT `requested` FROM `products` WHERE `id`='$id' LIMIT 1");
        $count = $result[0]->requested + 1;
        $this->request("UPDATE `products` SET `requested` = '$count' WHERE `id`='$id'");
    }

    public function addCategory($params){
        $result = $this->connector->prepare("INSERT INTO `categories` (`name`,`isVisible`) VALUES (?,?)");
        $result->execute(array($params['name'],$params['isVisible']));
        if (!$result){ print("Произошла ошибка при добавлении категории"); print_r($this->connector->errorInfo()); }
    }

    public function updateCategory($params){
        if ($params['id'] != ""){
            $result = $this->connector->prepare("UPDATE `categories` SET `name`=?, `isVisible`=? WHERE `id`=?");
            $result->execute(array($params['name'],$params['isVisible'],$params['id']));
            if (!$result){ print("Произошла ошибка при обновлении категории"); print_r($this->connector->errorInfo());}
        }
    }

    public function deleteCategory($id){
        $result = $this->connector->prepare("DELETE FROM `categories` WHERE `id`=?");
        $result->execute(array($id));
        if (!$result){print "Произошла ошибка при удалении категории";}
    }

    public function getCategories($params = "",$adminMode = false){
        $adminMode ? $query ="SELECT * FROM `categories`" : $query ="SELECT * FROM `categories` WHERE `isVisible`='1'";
        if ($params == ""){return $this->request($query); }
        else {
            $adminMode ? $query = $query." WHERE " : $query = $query." AND ";
            return $this->request($query."`id` IN (".$params.")"); }
    }

    public function getCategoryNameById($id){
        $res = $this->getCategories($id,true);
        return $res[0]->name;
    }

    public function addIngredient($data){
        if ($data != ""){
            $result = $this->connector->prepare("INSERT INTO `ingredients` (`name`) VALUES (?)");
            $result->execute(array($data['name']));
            if (!$result){ print("Произошла ошибка при добавлении ингредиента"); print_r($this->connector->errorInfo()); }
        }
    }

    public function updateIngredient($params){
        if ($params['id'] != ""){
            $result = $this->connector->prepare("UPDATE `ingredients` SET `name`=? WHERE `id`=?");
            $result->execute(array($params['name'], $params['id']));
            if (!$result){ print("Произошла ошибка при обновлении ингредиента"); print_r($this->connector->errorInfo()); }
        }
    }

    public function deleteIngredient($id){
        $result = $this->connector->prepare("DELETE FROM `ingredients` WHERE `id`=?");
        $result->execute(array($id));
        if (!$result){print "Произошла ошибка при удалении ингредиента";}
    }

    public function getIngredients($ingredientsList=""){
        $query = "SELECT * FROM `ingredients`";
        $ingredientsList = str_replace(" ", "", $ingredientsList);
        if ($ingredientsList == "") return;
        if ($ingredientsList != "") {$query.=" WHERE `id` IN (".$ingredientsList.")";}
        $result = $this->request($query);
        return $result;
    }

    public function getIngredientsList(){
        $query = "SELECT * FROM `ingredients`";
        $result = $this->request($query);
        return $result;
    }

    public function createOrder($data){
        $date = date("Y-m-d H:i:s");
        $this->add(
            array('name','phone','address','email','comment','products','created_time','state'),
            array($data['name'], $data['phone'], $data['address'], $data['email'], $data['comment'], $data['products'], $date, 0),
            'orders');

        $lastId = $this->getLastId();
        App::sendSMS($this->format_phone($data['phone']), "Ваш заказ №$lastId сформирован. Ожидайте!");
        return $lastId;
    }

    public function format_phone($phone){
        $phone = str_replace("+","", $phone);
        $phone = str_replace("(","", $phone);
        $phone = str_replace(")","", $phone);
        $phone = str_replace(" ","", $phone);
        $phone = str_replace("-","", $phone);

        return $phone;
    }

    public function getOrders($param = "", $limit = "", $order = ""){
        $query = "SELECT `id`,`address`, `created_time` as `created`, DATE_FORMAT(`created_time`,'%d.%m.%y в %H:%i') as `created_time`,
`email`,`name`,`phone`,`products`,`state`,`comment` FROM `orders`";
        //print $query;
        if ($param){ $query.=" WHERE ".$param;}
        if ($order != ""){ $query.=" ORDER BY ".$order;}
        if ($limit){ $query.=" LIMIT ".$limit;}
        //print($query);
        $result = $this->request($query);
        return $result;
    }

    public function updateOrderState($id,$param,$value){
        $this->request("UPDATE `orders` SET `$param` = '".$value."' WHERE `id`='$id'");
    }

    public function updateOrder($data){
        $date = date("Y-m-d H:i:s");
        $this->set(
            array('name','phone','address','email','comment','products','created_time','state'),
            array($data['name'], $data['phone'], $data['address'], $data['email'], $data['comment'], $data['products'], $date, 0),
            'orders',
            "`id` = '".$data['id']."'");
        $lastId = $this->connector->lastInsertId();
        print $lastId;
    }

    public function updateCart(){

    }

    /* NEWS ENGINE */
    public function getNews($id = null){
        if (!isset($id) || ($id == "")) {$query = ('SELECT * FROM `news` ORDER BY `date_published`');}
        else {$query = ("SELECT * FROM `news` WHERE `id`='$id'");}
        try {
            $result = $this->request($query);
        } catch (Exception $e){ $result = "no such entry"; }
        return $result;
    }

    /* USER ENGINE */
    public function getUsers(){
        $result = $this->request("SELECT * FROM `users`");
        if (!$result){print "Error while retrieving user"; } else { return $result; }
    }

    public function getUserByName($data){
        $result = $this->request("SELECT * FROM `users` WHERE `name`='$data' LIMIT 1");
        if (!$result){print "Error while retrieving user"; } else { return $result[0]; }
    }
}

?>
<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 17.01.2017
 * Time: 23:32
 */



class App {
    const logo_url = "logo.png";
    const company_name = "Центр Суши";
    const company_description = "Суши, Роллы, Пицца! Доставка бесплатно! Заказ от 450 рублей.";
    public $phone =  "8 (915) 321-77-77";
    public $phone2 = "8 (963) 780-60-05";
    public $phone3 = "8 (926) 932-06-08";
    public $address = "г. Коломна, улица Ленина, д. 69";

    public function address(){
        print $this->address;
    }

    public function phone(){
        print $this->phone;
    }

    public function phone2(){
        print $this->phone2;
    }

    public function phone3(){
        print $this->phone3;
    }

    public static function companyname(){
        print self::company_name;
    }

    public static function logo(){
        print self::logo_url;
    }

    public static function sendSMS($to,$message){
        $ch = curl_init("http://sms.ru/sms/send");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            "from"          => "CenterSushi",
            "api_id"		=>	"867ABB74-22CA-A7CD-5CAE-FAD3B35BAD43",
            "to"			=>	$to,
            "text"		=>	iconv("utf-8","utf-8",$message)

        ));
        $body = curl_exec($ch);
        curl_close($ch);
    }
}

?>
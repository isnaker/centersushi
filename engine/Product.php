<?php

abstract class Product {
    public $type = "";
    public $id = 0;
    public $name = "";
    public $price = 0;
    public $description = "";
    public $ingredients = array();
    public $image = "";
    public $requested = 0;

    function __construct($data){  }

    static function createInstance($object){
        // indicate classes to produce !!!!!!!!
        //return new Roll($object);

        if ($object->category === '12' || $object->category === '13'){
            return new Pizza($object);
        } else { return new Roll($object); }
    }

    static public function getIngredients(){   }

    public function incrementRequested(){
        $db = new DB();
        $db->incrementRequested($this->id);
    }

    public function makeDescriptionIslands(){
        $d = explode(",",$this->description);
        $res = "";
        foreach ($d as $dd){
            $res = $res."<span class='product-ingredient'>$dd</span>";
        }
        return $res;
    }

    function write(){}
    function writeModal(){}
    function save(){}
}
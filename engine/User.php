<?php

class User {
    public $name;

    public function __construct(){
        session_start();

        if (isset($_POST['action'])){
            if ($_POST['action'] == 'login'){ self::login($_POST);}
        }

        if (isset($_GET['action'])){
            if ($_GET['action'] == 'logout'){ self::logout(); }
        }


       if (!isset($_SESSION['user_autorized'])){
            header('Location: /admin/login.php');
        } else {
            $this->name = $_SESSION['user_autorized']->name;
       }
}

    public function login($data){
        $db = new DB();
        $user = $db->getUserByName($data['login']);

        if ($user->password == (md5($data['password']))){
            $_SESSION['user_autorized']=$user;
            print "<div id='userlogged'>пользователь ".$data['login']." авторизован успешно</div>";
            header("Location: /admin/index.php");
        }
        else {
            $_SESSION['message'] = "Юзер ".$data['login']." не авторизован - неверный логин или пароль";
        }
    }

    public static function logout(){
        unset($_SESSION['user_autorized']); $_SESSION['message'] = "Вы вышли из системы."; header("Location: /admin/login.php");
    }
}


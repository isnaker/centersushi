<?php
require('engine/config.php');
$app = new App();
$db = new DB();

$limiter = '6';
$needLimit = true;
$categoryId = "";
$cateGoryName = "";

if (isset($_GET['category'])){
    $categoryId = $_GET['category'];
    $limiter = '';
    $needLimit = false;
    $cateGoryName = $db->getCategoryNameById($categoryId)." : ";
}


?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php print $cateGoryName; ?> Доставка суши и роллов в Коломне. Центр Суши</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/product.css" type="text/css">
    <link rel="stylesheet" href="css/category.css" type="text/css">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name='yandex-verification' content='48de752d2a7ce431' />
    <meta name="keywords" content="Центр Суши, суши, роллы, заказать, заказать роллы Коломна, роллы Коломна, суши Коломна, заказать суши Коломна, заказать роллы, заказать суши, Суши ,доставка, суши, роллы, салаты, сашими, сеты, горячее, супы, десерты, напитки, Коломна, японская кухня, блюда, суши в коломне, доставка суши в коломне, заказать суши в Коломне"/>
    <meta name="description" content="Бесплатная доставка суши и роллов на дом в Коломне. Японские блюда от «Центр Суши» с доставкой."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />


    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/basic.js"></script>
    <script type="text/javascript" src="js/cart.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('nav a[href="category.php"]').addClass('current');

            $('.blocklist li').hide();
            $("#left-column .expander").click(function(){
               // $('.expanded').parent().next('.blocklist').hide(500);
               // $('.expanded').removeClass('expanded');

                $(this).toggleClass('expanded');
                var p = $(this).parent().next();
                if ($(this).hasClass('expanded')){

                    $(p).find('li').show(300);
                } else {
                    $(p).find('li').hide(500);
                }
            });
            $('nav a[href="category.php"]').addClass('current');
        });
    </script>
</head>

<body>
<?php include_once('header.php'); ?>
<div id="content">
    <div id="left-column">
        <div class="accordion">
        <?php
        $categories = $db->getCategories("",false);
        $products = $db->getProducts();

        foreach ($categories as $category){
            $products = $db->getProducts("`category` = $category->id","","`id`,`name`");
            if (count($products) > 0){
                print ("<span class='blocklist-head'><span class='expander'>$category->name</span> <a href='category.php?category=".$category->id."'>Все</a> </span><ul class='blocklist'>");
                foreach ($products as $product){
                    print "<li><a href='product.php?id=$product->id'>".$product->name."<span data-value='$product->id' class='quickAddToCart'>+</span></a></li>";
                }
                print ("</ul>");
            }
        }
        ?>
        </div>
    </div>
    <div id="right-column">
        <div class='breadcrumbs'>
            <a href='index.php'>Центр суши</a> >
            <a href='category.php'>Меню</a>
            <?php if (!$needLimit){print "> <a href='#'>".$db->getCategoryNameById($categoryId)."</a>";} ?>
        </div>
        <div id="main">
            <?php
                $categories = $db->getCategories($categoryId,false);
                if (!$categories){ print "No such category"; }
                foreach ($categories as $category){
                    $products = $db->getProducts("`category` = $category->id","","`name`");
                    if (count($products) > 0){
                        print "<div class='clearfix'></div><h2 class='category-name'>$category->name</h2>";
                        $limiter = 6;
                        if ($needLimit){
                            print "<a class='small-link' href='category.php?category=$category->id'>Все $category->name (".count($products).")</a>";
                            if (count($products) < $limiter) $limiter = count($products);
                        } else {
                            $limiter = count($products);
                        }

                        print "<div class='horizontal-content'>";
                        for($i=0; $i<$limiter; $i++){
                            $product = $products[$i];
                            $product->write();
                            if (($i+1) % 3 == 0){ print('<div class="clearfix"></div>'); }
                        }
                    print "</div>";
                    }
                }
            ?>
        </div>
    </div>
</div>

<?php include_once('footer.php'); ?>
</body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 10.01.2017
 * Time: 20:27
 */

$root = $_SERVER['DOCUMENT_ROOT'];
include_once($root."/engine/config.php");
include_once($root."/engine/Cart.php");

$page = new stdClass();
$page->title = "Центр Суши. Административная часть";

$app = new App();
$db = new DB();
$cart = new Cart();

if (isset($_GET['path'])){
    $path = $_GET['path'];

    switch ($path){
        case "orders":
            $page->title = "Администрирование заказов";
            $orders = $db->get('*','orders');
            $includable = array('/admin/engine/pages/orders.php');
            $add_js =array('/admin/js/orders.js');
            $add_css=array('/admin/css/orders.css');
            break;

        case "products":
            break;

        case "product":
            break;

        case "ingredients":

            break;

        case "home":
            break;

        default: break;
    }
}



include_once($root."/admin/engine/parts/template.php");
?>



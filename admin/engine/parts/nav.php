<nav class="navbar navbar-default navbar-static-top">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Центр Суши</a>
    </div>



    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="/admin/orders">Заказы <span class="sr-only">(current)</span> <span class="badge" data-value="count_new"> </a></li>
                <li><a href="/admin/products">Продукты</a></li>
                <li><a href="/admin/ingredients">Ингредиенты</a></li>
                <li><a href="/admin/actions">Акции</a></li>
                <li><a href="/admin/sliders">Слайдеры</a></li>
                <li><a href="/admin/stats">Статистика</a></li>
                <li><a href="/admin/settings">Настройки</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="/" target="_blank">На сайт</a></li>
            </ul>
        </div>
    </div>
</nav>
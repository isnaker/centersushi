<link rel="stylesheet" href="/css/framework.css" type="text/css">
<link rel="stylesheet" href="/css/style.css" type="text/css">
<link rel="stylesheet" href="/css/product.css" type="text/css">
<link rel="stylesheet" href="/css/product.preview.css" type="text/css">
<link rel="stylesheet" href="/css/admin.css" type="text/css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<link rel="icon" href="/img/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
<meta name="keywords" content="Центр Суши, суши, роллы, заказать, заказать роллы Коломна, роллы Коломна, суши Коломна, заказать суши Коломна, заказать роллы, заказать суши, Суши ,доставка, суши, роллы, салаты, сашими, сеты, горячее, супы, десерты, напитки, Коломна, японская кухня, блюда, суши в коломне, доставка суши в коломне, заказать суши в Коломне"/>
<meta name="description" content="Бесплатная доставка суши и роллов на дом в Коломне. Японские блюда от «Центр Суши» с доставкой."/>
<script type="text/javascript" src="/js/jquery-1.11.0.min.js"></script>

<script type="text/javascript" src="/js/basic.js"></script>
<script type="text/javascript" src="/js/cart.js"></script>
<script type="text/javascript" src="../../js/main.js"></script>
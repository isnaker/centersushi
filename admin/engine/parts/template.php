<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?= $page->title ?></title>

    <!-- Bootstrap -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/admin/css/admin.css" rel="stylesheet">


    <?php if (isset($add_css)) {
        foreach ($add_css as $item){ ?>
            <link href="<?= $item; ?>" type="text/css" rel="stylesheet">
        <?php }
    } ?>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

</head>
<body>

<?php include_once($_SERVER['DOCUMENT_ROOT']."/admin/engine/parts/nav.php"); ?>


<div id="content">
    <?php if (isset($content)) {print $content; } ?>
    <?php if (isset($includable)){
        foreach($includable as $item)        {   include_once($_SERVER['DOCUMENT_ROOT'].$item);        }
    } ?>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/admin/js/bootstrap.min.js"></script>

<?php if (isset($add_js)) {
    foreach ($add_js as $item){ ?>
        <script src="<?= $item; ?>" type="text/javascript"></script>
    <?php }
} ?>

</body>
</html>
<?php
include_once('../engine/config.php');
$db = new DB();
$user = new User();

if (isset($_POST)){
    switch ($_POST['action']){
        case "writeIngredient":
            if ($_POST["id"] != ""){ $db->updateIngredient($_POST); } else { $db->addIngredient($_POST); }
            break;
    }
}

if (isset($_GET['action'])){
    switch ($_GET['action']){
        case "deleteIngredient":
            $db->deleteIngredient($_GET['id']);
            break;
    }
    header('Location: ingredients.php');
}

$ingredients = $db->getIngredientsList();
$products = $db->getProducts(null,null,null,true);
?>
<html>
<head>
    <title>Центр Суши</title>
    <?php include_once('header.php'); ?>
    <style>
        .item {font-size: 1.2em}
        #search {width: 46%; font-size: 1.2em; padding: 8px 14px; margin-bottom: 0.5em}
        .edit {font-size: 0.6em; opacity: 0.3; color: #666666; position: relative; top: -3px}
        .edit:hover {cursor: pointer; opacity: 1}
        #editIngredientForm {
            min-width: 300px;
            margin: 15% 33%;
            left: 0;
            right: auto;
            bottom: auto;
            top: 0;
            overflow: hidden;
        }
        #editIngredientForm h2 {margin-bottom: 8px}
        #editIngredientForm .minimalicbutton {
            margin-top: 10px;
        }
        #editIngredientForm .row, .formControls {padding-left: 10%;}
        #deleteIngredient {float: right; font-size: 0.6em; border-bottom: dotted black 1px}
        #deleteIngredient:hover {color: red}
    </style>
    <script>
        var ingredients;
        ingredients = <?php print json_encode($ingredients); ?>;
        $(function(){
            $('#cancelIngredientEditing').click(function(){
                $('#editIngredientForm').addClass('hidden');
                return false;
            });

            $('#addNewIngredient').click(function(){
                $('#id').attr('value', "");
                $('#name').attr('value', "");
                $('#deleteIngredient').css('display','none');
                $('#editIngredientForm').removeClass('hidden');
            });

            $('#ingredientList .item .edit').click(function(){
                var id = parseInt($(this).attr('data-value'));

                var ingredient;
                for (var i=0; i<ingredients.length; i++){
                    if (id == ingredients[i].id) {ingredient = ingredients[i]; }
                }
                $('#id').attr('value', id);
                $('#name').attr('value', ingredient.name);
                $('#deleteIngredient').attr('href', '?action=deleteIngredient&id='+ingredient.id).css('display','inline-block');;

                $('#editIngredientForm').removeClass('hidden');
            });

            $('#search').on('input', function() {
                if ($(this).val() == "") { $('#ingredientList .item').css('opacity','1'); }
                else { $('#ingredientList .item').css('opacity','0.1'); }

                var needle = $(this).val();
                var searchable = $('#ingredientList .item');
                for (var i=0; i < searchable.length; i++){
                    var txt = searchable[i].innerHTML;
                    var found = txt.match(needle);
                    found ? searchable[i].style.opacity = 1 : "";

                }
            });
        });
    </script>
</head>
<body>
<?php include_once('../engine/config.php'); ?>
<?php include_once('section_control.php'); ?>

<div id="content">
    <div id='left-column'>
        <img id='section_switcher' src='/img/icon-menu.png' alt=''>
        <div  style="background: #ffffff; margin: 10px 0 25px 0;padding: 5%">
            Здесь вы можете управлять ингредиентами, из которых состоят блюда в меню. </div>
    </div>

    <div id="right-column">
        <h1 class="box33p"><i class="fa fa-flask"></i> Ингредиенты </h1>
        <div class="clearfix"><input type="text" id="search" value="" placeholder="Фильтр ингредиентов">  </div>
        <div id="ingredientList">
            <button id="addNewIngredient" class="minimalicbutton"><i class="fa fa-plus-circle"></i> Добавить новый ингредиент</button>
            <div class="clearfix"><hr></div>
        <?php
            foreach ($ingredients as $ingredient){
                print "<div class='item' data-value='$ingredient->id'>$ingredient->name
                <small data-value='$ingredient->id' class='edit'><i class='fa fa-pencil'></i></small></div>";
            }
        ?>
        </div>
    </div>
</div>

<div id="editIngredientForm" class="floatform text-left hidden">
    <form action="ingredients.php" id="newProduct" enctype="multipart/form-data" method="post">
        <h2><i class="fa fa-plus-circle"></i> Добавить/изменить ингредиент</h2>
        <a id="deleteIngredient" href="">Удалить ингредиент</a>
        <div class="row">
            <input hidden="hidden" name="action" value="writeIngredient">
            <input hidden="hidden" name="id" id="id" value="">
            <label for="name">Название ингредиента</label>
            <input name="name" id="name" value="">
        </div>

        <div  class="formControls clearfix">
            <input type="submit" class="minimalicbutton" value="Сохранить изменения">
            <button class="minimalicbutton" id="cancelIngredientEditing">Отмена</button>
        </div>
    </form>
</div>


</body>
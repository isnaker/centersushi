<?php
    include_once($_SERVER['DOCUMENT_ROOT'] . '/engine/config.php');
    include_once($_SERVER['DOCUMENT_ROOT'] . '/admin/engine/classes/Simpleimage.php');
    $db = new DB();
    $user = new User();

        if (isset($_POST['action'])){
            switch ($_POST['action']){
                case "writeProduct":
                    if ($_POST["id"] != ""){ $db->updateProduct($_POST); } else { $db->addProduct($_POST); }
                    break;
            }
        }

        if (isset($_GET['action'])){
            switch ($_GET['action']){
                case "deleteProduct":
                    //print "del product ".$_GET['id'];
                    $db->deleteProduct($_GET['id']);
                    break;
            }
            header('Location: /admin/engine/pages/deprecated/products.php');
        }
    $products = $db->getProducts(null,null,null,true);
?>
<html>
<head>
    <title>Продукты. Доставка суши и роллов в Коломне. Центр Суши</title>
    <?php include_once($_SERVER['DOCUMENT_ROOT'].'/admin/engine/parts/header.php'); ?>
    <script>
        var products = [];
        <?php
            $str = json_encode($products);
            print "products = $str;";
        ?>

        function composeIngredients(){
            var data = $('#ingredients .selected');
            var ingredientsString = "";
            for (var i=0; i<data.length; i++){
                ingredientsString+=data[i].getAttribute('data-value');
                (i != data.length-1) ? ingredientsString+=',' : '' ;
            }
            $('#ingredients').val(ingredientsString);
        }

        $(function(){
            $('#search').on('input', function() {

                if ($(this).val() == "") { $('#productList .item').css('display','inline-block'); }
                else { $('#productList .item').css('display','none'); }

                var needle = $(this).val().toLowerCase();
                var searchable = $('#productList .item');
                for (var i=0; i < searchable.length; i++){
                    var txt = searchable[i].innerHTML.toLowerCase();
                    var found = txt.match(needle);
                    found ? searchable[i].style.display = "inline-block" : "";
                }
            });

            $('.checkbox').click(function(){
                $(this).toggleClass('selected');
            });

            $('.ingredient').click(function(){
                composeIngredients();
            });

            $('#addNewProduct').click(function(){
                $('#addProductForm').removeClass('hidden');
                $('#addProductForm .input').val("");
                $('#addProductForm .selected').removeClass('selected').attr('checked', "");
            });

            $('#cancelOrderCreation').click(function(){
               $('#addProductForm').addClass('hidden');
                $('#id').attr('value', "");
                $('#ingredients .selected').removeClass('selected');
                return false;
            });

            $('.item .edit').click(function(){
                var id = parseInt($(this).attr('data-value'));
                $('#id').attr('value', id);
                var product;
                for (var i=0; i<products.length; i++){
                    if (id == products[i].id) {product = products[i]; }
                }

                $('#name').val(product.name);
                $('#price').val(product.price);
                $('#weight').val(product.weight);
                $('#details').html(product.description);
                $('#category').val(product.category);
                $('#image').attr('src','/img/products/300/'+product.image);
                //ingredients check
               // var ingredients = $('#ingredients .ingredient');
                for (var i=0; i<product.ingredients.length; i++){
                    $('#ingredients .ingredient[data-value="' + parseInt(product.ingredients[i].id) + '"]').addClass('selected');
                }
                composeIngredients();
                $('#addProductForm').removeClass('hidden');
            });

            $('#viewAsList').click(function(){
                $('#viewAsGrid').removeClass('selected');
                $(this).addClass('selected');
               $('#productList .item').addClass('asList');
            });

            $('#viewAsGrid').click(function(){
                $('#viewAsList').removeClass('selected');
                $(this).addClass('selected');
               $('#productList .item').removeClass('asList');
            });

        });
    </script>
    <style>
        #productList .item {width: 168px; float: left; min-height: 300px}
        #productList .item h4 {line-height: 0.9;padding-top: 0.33em; font-size: 0.7em}
        #productList .item .item-foto a {font-size: 0.5em;}
        #productList .item .item-foto a:last-child {float: right}

        #productList .item.asList {width: 96%; display: block; min-height: 20px; margin: 1px}
        #productList .item.asList img {display: none}
        #search {width: 46%; font-size: 1.2em; padding: 8px 14px; margin-bottom: 0.5em}


        #ingredients {font-size: 0.75em;background: rgba(0,0,0,0.05); width: 101%; margin-left: -2%; margin-top: 1%; padding:0.5% 0 2% 2%}
        #ingredients h2 {margin-bottom: 8px}

        #viewAsList:hover, #viewAsGrid:hover {color: gray; cursor: pointer}
        #viewAsList.selected, #viewAsGrid.selected {color: #e6373c }
    </style>
</head>
<body>

<?php include_once('section_control.php'); ?>
<div id="content">
    <div id='left-column'>
        <img id='section_switcher' src='/img/icon-menu.png' alt=''>

        <div class="row" style="background: #ffffff; padding: 8px 15px; margin: 10px 0 25px 0">
            Здесь вы можете управлять продуктами. Добавление, изменение или удаление продуктов реализовано максимально понятным образом.
        Вы можете отфильтровать товары, начав набирать название товара в строку "Фильтр продуктов". Каждый товар, кроме всего прочего, может быть скрыт от показа посетителям.</div>
    </div>

    <div id="right-column">
        <h1 class="box33p"><i class="fa fa-cubes"></i> Продукты </h1>


    <div class="clearfix">
        <input type="text" id="search" value="" placeholder="Фильтр продуктов">  </div>
        <div id="productList">
            <button id="addNewProduct" class="minimalicbutton"><i class="fa fa-plus-circle"></i> Добавить новый продукт</button>

            <div class="clearfix">
                <div id="viewControl" class="text-right">
                    <a id="viewAsList"><i class="fa fa-th-list"></i></a>
                    <a id="viewAsGrid" class="selected"><i class="fa fa-th-large"></i></a>
                </div>
                <hr></div>
            <?php
            foreach ($products as $product){
                $category = $db->getCategories("$product->category",true);
                $category = $category[0];
                $hidden = "";
                if ((int)$product->isVisible === 0) {$hidden = " overlined"; }

                print "<div class='item' data-value='$product->id'>
                        <a href='products.php?action=deleteProduct&id=$product->id' title='Удалить товар' class='fa fa-times del'></a>
                        <a style='margin: 0 0 0 37.5%; display: inline-block;font-size: 0.6em' title='Просмотр (в новом окне)' target='_blank' href='/product.php?id=$product->id'><i class='fa fa-eye'></i></a>
                    <small class='edit' data-value='$product->id' title='Редактировать'><i  class='fa fa-pencil'></i></small>

                    <h4 class='clearfix$hidden'>$product->name</h4>
                    <small>$category->name</small>
                    <div class='item-foto'>
                        <img class='flexible' src='/img/products/300/$product->image' alt='$product->name'>
                        <a style='display: none' download class='text-centered' href='$product->image'>Скачать фото</a>
                        <a style='display: none' class='loadNewPhoto'>Изменить фото</a>
                    </div>
                </div>";
            }
            ?>
        </div>
    </div>
    <div class="clearfix"></div>

    <div id="addProductForm" class="floatform text-left hidden">
        <form action="products.php" id="newProduct" enctype="multipart/form-data" method="post">
            <h2><i class="fa fa-plus-circle"></i> Добавить/изменить продукт</h2>

            <div class="row">
            <div class="box30p">
                <h3>Данные товара</h3>
            <input hidden="hidden" name="action" value="writeProduct">
            <input hidden="hidden" name="id" id="id" value="">
            <div>
                <label for="name">Название</label>
                <input class="input" id="name" name="name">
            </div>
            <div>
                <label for="price">Стоимость</label>
                <input class="input" width="10" size="10" name="price" id="price" placeholder="price">
            </div>
            <div>
                <label for="weight">Вес</label>
                <input class="input" width="10" size="10" name="weight" id="weight" placeholder="weight">
            </div>
            <div>
                <label for="rating">Рейтинг</label>
                <input class="input" width="10" size="10" name="rating" id="rating" placeholder="rating" value="0">
            </div>

            </div>

            <div class="box30p" style="padding-top: 28px">
                <div>
                    <label for="category">Категория</label>
                    <select id="category" name="category">
                        <?php
                        $categories = $db->getCategories();
                        foreach ($categories as $category){
                            print "<option type='checkbox' value='$category->id'>$category->name</option>";
                        }
                        ?>
                    </select>
                </div>
                <div>
                    <label for="details">Описание</label>
                    <textarea name="details" id="details" placeholder="details"></textarea>
                </div>
                <div>
                    <input class="input" hidden="hidden" id="ingredients" name="ingredients" placeholder="ingredients">
                </div>
                <h4>Видимость товара</h4>
                <select name="isVisible">
                    <option value="1">Виден пользователям</option>
                    <option value="0">Скрыт от просмотра</option>
                </select>
            </div>
            <div class="box25p">
                <h3>Изображение товара</h3>
                <img class="flexible" id="image" src="/img/products/product-example.jpg" style="max-height: 120px">
                <input class="input" type="file" name="image">
            </div>
                <div class="clearfix"></div>
            </div>

            <div id="ingredients" class="row">
                <h2>Добавьте ингредиенты к продукту</h2>
                <?php
                $ingredients = $db->getIngredientsList();
                foreach ($ingredients as $ingredient){
                    print "<div class='checkbox ingredient' data-value='$ingredient->id'><i class='fa fa-check'></i>$ingredient->name</div>";
                }
                ?>
                <div class="clearfix"></div>
            </div>

            <div class="clearfix" class="formControls">
                <input type="submit" class="minimalicbutton" value="Сохранить изменения">
                <button class="minimalicbutton" id="cancelOrderCreation">Отменить</button>
            </div>
        </form>
    </div>
</div>
</body>
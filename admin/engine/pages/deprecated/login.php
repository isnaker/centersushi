<?php
include($_SERVER['DOCUMENT_ROOT'].'/engine/config.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/admin/engine/parts/header.php');
?>
<html>
<head>
    <style>
        #loginForm {background: #ffffff; max-width: 600px; margin-top: 10%; padding: 3% 0}
        #loginForm input[type='submit'] {display: block; width: 100%}
        .message {background: #ffffff; max-width: 600px; padding: 1% 0; margin-top: 1%}
        #passRetrieve {font-size: 0.7em; margin-top: 0.4em; display: none}
    </style>
</head>
<body>

<div id="loginForm" class="centered text-centered">
    <div class="box50p text-centered">
        <img class="flexible" src="/img/<?php App::logo(); ?>">
    </div>
    <div class="box5p"></div>
    <div class="box33p">

        <form action="index.php" method="post">
            <input hidden="hidden" name="action" value="login">
            <h3>Логин</h3>
            <div><input name="login" type="text"> </div>
            <h3>Пароль</h3>
            <div><input name="password" type="password"> </div>
            <div><input class="minimalicbutton" value="Войти в систему" type="submit"></div>
            <div><a class="small" id="passRetrieve">Восстановление пароля</a></div>
        </form>

    </div>
    <div class="clearfix"></div>
</div>

<?php if (isset($_SESSION['message'])){ print("<div class='message text-centered centered'>".$_SESSION['message']."</div>"); } ?>

</body>

</html>
<?php
include_once('../engine/config.php');
$db = new DB();
$user = new User();

if (isset($_POST)){
    switch ($_POST['action']){
        case "writeCategory":
            if ($_POST["id"] != ""){ $db->updateCategory($_POST); } else { $db->addCategory($_POST); }
            break;
    }
}

if (isset($_GET['action'])){
    switch ($_GET['action']){
        case "deleteCategory":
            $db->deleteCategory($_GET['id']);
            break;
    }
    header('Location: categories.php');
}

$categories = $db->getCategories("",true);
?>
<html>
    <head>
        <title>Категории. Доставка суши и роллов в Коломне. Центр Суши</title>
        <?php include_once('header.php'); ?>
        <style>
            .item {display: block; width: 98%}
            .item h4 {display: inline-block; margin-left: 2%}
            .edit {float: left;}
            .item .del {float: right}
            #deleteCategory {float: right; font-size: 0.6em; border-bottom: dotted black 1px}
            #deleteCategory:hover {color: red}
            #editCategoryForm {
                min-width: 300px;
                margin: 15% 33%;
                left: 0;
                right: auto;
                bottom: auto;
                top: 0;
                overflow: hidden;
            }
            #search {width: 46%; font-size: 1.2em; padding: 8px 14px; margin: 0.5em 0 0.2em 0}
            #editCategoryForm h2 {margin-bottom: 8px}
        </style>
        <script>
            var categories = <?php print (json_encode($categories)); ?>;
            $(function(){
                $('#cancelCategorytEditing').click(function(){
                    $('#editCategoryForm').addClass('hidden');
                    return false;
                });

                $('#addNewCategory').click(function(){
                    $('#id').attr('value', "");
                    $('#name').attr('value', "");
                    $('#deleteCategory').css('display','none');
                    $('#editCategoryForm').removeClass('hidden');
                });

                $('#categoryList .item .edit').click(function(){
                    var id = parseInt($(this).attr('data-value'));
                    var category;
                    for (var i=0; i<categories.length; i++){
                        if (id == categories[i].id) {category = categories[i]; }
                    }
                    $('#id').attr('value', id);
                    $('#name').attr('value', category.name);
                    $('#isVisible option[value="'+category.isVisible+'"]').attr('selected',"selected");

                    $('#deleteCategory').attr('href', '?action=deleteCategory&id='+category.id).css('display','inline-block');;

                    $('#editCategoryForm').removeClass('hidden');
                });
            })
        </script>
    </head>
    <body>
    <?php include_once('section_control.php'); ?>
    <div id="content">
        <div id='left-column'>
            <img id='section_switcher' src='/img/icon-menu.png' alt=''>
            <div class="row" style="background: #ffffff; padding: 8px 15px; margin: 10px 0 25px 0">
                Здесь вы можете управлять категориями продуктов.
                Категории не отображаются на сайте, если в них нет хотя бы одного продукта.
                Также можно сделать категорию скрытой, используя переключатель видимости.
                В этой версии продукта вложенные категории не реализованы.</div>
        </div>

        <div id="right-column">
            <h1 class="box33p"><i class="fa fa-th"></i> Категории товаров</h1>
            <div id="categoryList" class="clearfix">
                <button id="addNewCategory" class="minimalicbutton"><i class="fa fa-plus-circle"></i> Добавить новую категорию</button>
                <hr>
            <?php
            foreach ($categories as $category){
                $hidden = "";
                if ((int)$category->isVisible === 0) {$hidden = " overlined"; }
                print "<div class='item' data-value='$category->id'>
                        <a href='?action=deleteCategory&id=$category->id' title='Удалить категорию' class='fa fa-times del'></a>
                        <small class='edit' data-value='$category->id' title='Редактировать'><i  class='fa fa-pencil'></i></small>
                    <h4 class='clearfix$hidden'>$category->name</h4>
                </div>";
            }
            ?>
        </div>
    </div>

    <div id="editCategoryForm" class="floatform text-left hidden">
        <form action="categories.php" id="newCategory" enctype="multipart/form-data" method="post">
            <h2><i class="fa fa-plus-circle"></i> Добавить/изменить категорию</h2>
            <a id="deleteCategory" href="">Удалить категорию</a>
            <div class="row">
                <input hidden="hidden" name="action" value="writeCategory">
                <input hidden="hidden" name="id" id="id" value="">

                <label for="name">Название категории</label>
                <input name="name" id="name" value="">

                <select name="isVisible" id="isVisible">
                    <option value="1">Видна пользователям</option>
                    <option value="0">Скрыта</option>
                </select>
            </div>

            <div  class="formControls clearfix">
                <input type="submit" class="minimalicbutton" value="Сохранить изменения">
                <button class="minimalicbutton" id="cancelCategorytEditing">Отмена</button>
            </div>
        </form>
    </div>

</body>

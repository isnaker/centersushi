<div id="section_control">
    <div class="logo"><img src="/img/<?php print App::logo_url; ?>" alt=""> </div>
    <a href="index.php" class="item">Панель управления</a>
    <a href="orders.php" class="item">Заказы</a>
    <a href="products.php" class="item">Продукты</a>
    <a href="ingredients.php" class="item">Ингредиенты</a>
    <a href="categories.php" class="item">Категории</a>
    <div class="item disabled">Статистика</div>
    <div class="item disabled">Новости</div>
    <div class="item disabled">Отзывы</div>
    <a href="settings.php" class="item">Настройки</a>
</div>
<html>
<head>
    <title>Настройки. Доставка суши и роллов в Коломне. Центр Суши</title>
    <?php
        include_once('../engine/config.php');
        $db = new DB();
        $user = new User();

        include_once('header.php');
    ?>
    <style>
        #settings {
            background: #ffffff;
            min-height: 200px;
            margin-top: 1em;
            padding: 2% 3%;
        }

        #settings h2 {margin-bottom: 1em}
        #settings h4 {margin: 0.3em 0}
        .checkbox .fa {opacity: 0.08}
        textarea {display: block; width: 70%; min-height: 90px}
        .minimalicbutton {margin-top: 0.3em}
        hr {margin-top: 2em}
        .description {margin-left: 8%; font-size: 0.8em}
    </style>
    <script>
        $(function(){
            $('.checkbox').click(function(){
                $(this).toggleClass('selected');
            });
        });
    </script>
</head>
<body>
<?php include_once('section_control.php'); ?>
<div id="content">
    <div id='left-column'>
        <img id='section_switcher' src='/img/icon-menu.png' alt=''>
    </div>

    <div id="right-column">
        <h1><i class="fa fa-gears"></i> Настройки</h1>
        <div id="settings" class="clearfix">

            <div class="box30p">
                <h2>Общие настройки</h2>
                    <div class="checkbox"><i class="fa fa-check"></i>Звуковые оповещения</div>
                    <div class="clearfix box50p description"><small>Включить звуковое оповещение при поступлении нового заказа</small></div>
            </div>
            <div class="box5p"></div>
            <div class="box30p">
                <h2>Система</h2>
                <h4>Название компании</h4>
                <div><input type="text" value="<?php App::companyname() ?>"></div>
                <h4>Описание компании</h4>
                <div><textarea><?php print App::company_description; ?></textarea></div>
            </div>
            <div class="box5p"></div>
            <div class="box30p">
                <h2>Логотип компании</h2>
                <img src="/img/<?php App::logo(); ?>">
                <h4>Иконка</h4>
                <img style="margin-left: 20px" width="20" src="/img/<?php App::logo(); ?>">
            </div>
            <div class="box30p clearfix">
                <h2>Пользователи системы</h2>
                <div id="users">
                    <?php
                    $users = $db->getUsers();
                    foreach ($users as $user){
                        print "<div>".$user->name;
                        print $user->active."</div>";
                    }

                    ?>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</body>
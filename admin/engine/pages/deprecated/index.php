<html>
<head>
    <title>Доставка суши и роллов в Коломне. Центр Суши </title>
    <link rel="stylesheet" href="/css/framework.css" type="text/css">
    <link rel="stylesheet" href="/css/style.css" type="text/css">
    <link rel="stylesheet" href="/css/product.css" type="text/css">
    <link rel="stylesheet" href="/css/product.preview.css" type="text/css">
    <link rel="stylesheet" href="/css/admin.css" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="icon" href="/img/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon"/>

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="keywords" content="Центр Суши, суши, роллы, заказать, заказать роллы Коломна, роллы Коломна, суши Коломна, заказать суши Коломна, заказать роллы, заказать суши, Суши ,доставка, суши, роллы, салаты, сашими, сеты, горячее, супы, десерты, напитки, Коломна, японская кухня, блюда, суши в коломне, доставка суши в коломне, заказать суши в Коломне"/>
    <meta name="description" content="Бесплатная доставка суши и роллов на дом в Коломне. Японские блюда от «Центр Суши» с доставкой."/>
    <script type="text/javascript" src="/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/js/basic.js"></script>
    <script type="text/javascript" src="/js/cart.js"></script>
    <style>
        #content .logo {margin-top: 12px;position: relative}
        #gotosite {position: absolute}
        h2 {margin: 25px 0}
        #controls .item {
            position: relative;
            display: inline-block;
            padding: 2% 0;
            background: #ffffff;
            width: 23%;
            margin: 1%;
            box-shadow: 0 1px 3px rgba(0,0,0,0.16);
            transition: all 0.3s ease;
            -webkit-transition: all 0.3s ease;
            font-size: 1.4em;
        }
        #controls a.item.active:hover {
            background: #d14c3f;
            color: #ffffff;
            cursor: pointer;
        }

        #controls a .fa {display: block;font-size: 2em; color: #AAA}
        #controls a.item.active:hover .fa {color: white}
        #controls #logout_button {clear: both; margin-top: 100px; font-size: 0.9em; padding: 1% 2%}
        .sticker { display: none; background: gold; border: cornsilk dotted 2px; border-radius: 50%; position: absolute; top: -20px; right: -15px; padding: 3px 14px }
        #userlogged {position: absolute; top: 0; width: 100%; padding: 1em 0; text-align: center; z-index: 8; background: #ffffff}
    </style>
    <script>
        $(function(){
            setTimeout( "_hide()", 2000 );
        });

        function _hide(){ $('#userlogged').hide(500); }
    </script>
</head>
<body>
<?php

    include('../engine/config.php');
    $db = new DB();
    $user = new User();
?>

<div id="content">
    <div class="logo">
        <img src="/img/<?php print App::logo_url; ?>" alt="">
        <a title="Перейти на сайт" target="_blank" id="gotosite" href="/"><i class="fa fa-external-link"></i></a>
    </div>
    <h2>Панель управления</h2>
    <div class="box1000 centered" id="controls">
        <a href="orders.php" class="item active"> <span class="sticker">3</span> <i class="fa fa-rouble"></i>Заказы</a>
        <a href="products.php" class="item active"><i class="fa fa-cubes"></i>Продукты</a>
        <a href="ingredients.php" class="item active"><i class="fa fa-flask"></i>Ингредиенты</a>
        <a href="categories.php" class="item active"><i class="fa fa-th"></i>Категории</a>
        <a class="item disabled"><i class="fa fa-gift"></i>Акции</a>
        <a class="item disabled"><i class="fa fa-line-chart"></i>Статистика</a>
        <a class="item disabled"><i class="fa fa-newspaper-o"></i>Новости</a>
        <a class="item disabled"><i class="fa fa-smile-o"></i>Отзывы</a>
        <a href="settings.php" class="item active"><i class="fa fa-gears"></i>Настройки</a>

        <div class="clearfix"> <a class="item active" id="logout_button" href="?action=logout">выйти из системы</a></div>
    </div>

    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
</body>
</html>
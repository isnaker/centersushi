<html>
<head>
    <title>Доставка суши и роллов в Коломне. Центр Суши</title>
    <?php include_once('header.php'); ?>
    <script type="text/javascript" src="/admin/js/orders.js"></script>
    <style>
        h1 {margin: 0.4em 0}
        #order-loader {margin-left: 3%; transition: all 0.5s ease; -webkit-transition: all 0.5s ease;}
        #modalOrder {position: fixed; background: #ffffff; width: 33%; top: 250px; left: 32%; z-index: 9; padding: 1%; box-shadow: 0 1px 5px rgba(0,0,0,0.33); transition: all 0.5s ease; -webkit-transition: all 0.5s ease;}
        .cart-item .edit {position: absolute; right: 5px; bottom: 5px; color: gray}
        .cart-item .edit:hover {cursor: pointer; color: #e6373c}
        .edit {top: auto}
    </style>
</head>
<body>
<audio id="audio" preload="auto">
    <source src="../../../sounds/neworder.wav"></source>
</audio>

<?php
      include_once('../engine/config.php');
      include_once('section_control.php');
      $db = new DB();
      $user = new User();
?>

<div id="content">
    <div style='background: white; padding: 3px 0; box-shadow: 0 2px 4px rgba(0,0,0,0.33);z-index: 2;position: relative; '>
        <button style='position: absolute;left: 15px;top: 0' class='minimalicbutton' id='addAnOrder'>+ Добавить заказ</button>
        <h1>Администрирование заказов</h1>
        <a href='?action=logOut' style='position: absolute;right: 15px;top: 0px' class='minimalicbutton' id='logOut'>Выйти</a>
        <div class='clearfix'></div>
    </div>

    <div id='left-column'>
        <img id='section_switcher' src='/img/icon-menu.png' alt=''>
    </div>

    <div id='right-column'>
        <div id='navigation'>
            <span data-value='-1' class='nav-item current'>Все заказы</span>
            <span data-value='0' class='nav-item'>Только новые</span>
            <span data-value='1' class='nav-item'>В обработке</span>
            <span data-value='2' class='nav-item'>Выполненные</span>
            <span data-value='3' class='nav-item'>Отмененные</span>

            <span id='order-loader' class='transparent'><i class='fa fa-circle-o-notch fa-spin'></i> Заказы обновляются...</span>
        </div>
            <div id='orders'></div>
        <div class='clearfix'></div>
</div>
<div class='clearfix'></div>
<div id="modalOrder" class="hidden">
    <h2 id="orderAction">Изменение заказа № <span id="orderNumber"></span> </h2>
        <div><input id="orderClient" value="Олеся"></div>
        <div><input id="orderPhone" value="8 916 525 63 69"></div>
        <div><input id="orderEmail" value="ol@ol.com"></div>
        <div><textarea id="orderComment">Подвезите к ПЛАЗМЕ плз</textarea></div>
        <div id="orderProducts"></div>
    <h2 id="orderPrice"></h2>
    <button id="orderOk" class="minimalicbutton">OK</button>
</div>
    <div> Вы вошли как <?php print $user->name; ?> </div>
</div>

</body>
</html>
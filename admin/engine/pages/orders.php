<div class="container">
    <div class="row">
        <div class="col-md-4">
            <p class="h4 mt-none">Управление заказами
                    <button
                        class="btn btn-default"
                        data-action="newOrder">Добавить заказ</button>
            </p>
        </div>
        <div class="col-md-4">

        </div>
        <div class="col-md-4 text-right">
                <span data-value="orders_count"></span> Заказов
                <button class="btn btn-default" data-action="getOrders" type="button">Обновить заказы</button>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs mb-lg" id="tabs">
                <li role="presentation" class="active">
                    <a href="#tab-new" aria-controls="home" role="tab" data-toggle="tab-new">Новые
                        <span class="badge" data-value="count_new"></span>
                    </a>
                </li>
                <li role="presentation"><a href="#tab-working" aria-controls="home" role="tab" data-toggle="tab-working">Выполняются
                        <span class="badge" data-value="count_working"></span>
                    </a></li>
                <li role="presentation"><a href="#tab-completed" aria-controls="home" role="tab" data-toggle="tab-completed">Выполнены
                        <span class="badge" data-value="count_completed"></span>
                    </a></li>
                <li role="presentation"><a href="#tab-cancelled" aria-controls="home" role="tab" data-toggle="tab-cancelled">Отменены
                        <span class="badge" data-value="count_cancelled"></span>
                    </a></li>
                <li role="presentation"><a href="#tab-all" aria-controls="home" role="tab" data-toggle="tab-all">Все
                        <span class="badge" data-value="orders_count"></span>
                    </a></li>

            </ul>
        </div>
    </div>

    <div class="row">
        <div id="orders-container" class="col-md-4">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane " id="tab-all"></div>
                <div role="tabpanel" class="tab-pane active" id="tab-new"></div>
                <div role="tabpanel" class="tab-pane " id="tab-working"></div>
                <div role="tabpanel" class="tab-pane" id="tab-completed"></div>
                <div role="tabpanel" class="tab-pane" id="tab-cancelled"></div>
            </div>
        </div>
        <div class="col-md-8 hidden" id="order-details">
            <div class="row">
                <div class="col-md-4">

                    <P class="H2"><strong>Детали заказа <span data-value="order_id"></span></strong></P>
                </div>
                <div class="col-md-4"><p class="text-right">Создан <span data-value="date_created"></span></p></div>

                <div class="col-md-4 text-right">
                    <div class="btn-group mt-none">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="state-class" data-value="order_state"></span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#" data-toggle="orderState" data-value="0">Новый</a></li>
                            <li><a href="#" data-toggle="orderState" data-value="1">В обработке</a></li>
                            <li><a href="#" data-toggle="orderState" data-value="2">Выполнен</a></li>
                            <li><a href="#" data-toggle="orderState" data-value="3">Отменен</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Клиент:</label>
                        <input data-value="name" type="text" class="form-control">
                    </div>

                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="">Адрес доставки:</label>
                        <textarea data-value="address" class="form-control"></textarea></div>
                </div>

            </div>
            <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Телефон клиента:</label>
                            <input data-value="phone" type="tel" class="form-control">
                        </div>
                    </div>
                  <div class="col-md-8">
                    <div class="form-group">
                        <label for="">Комментарий к заказу:</label>
                        <textarea data-value="comment" class="form-control"></textarea></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div id="addProductPanel" class="hidden">
                        <div class="heading">
                                <i class="fa fa-times" data-action="close"></i>
                        </div>
                        <div class="list">
                            <div class="text-center"><i class="fa fa-spin fa-spinner"></i></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <p class="h4">Состав заказа: </p>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-default pull-right" data-action="toggleProductPanel">Добавить продукт</button>
                        </div>
                    </div>


                    <table class="table table-bordered mt-lg" data-value="products"></table>
                    <p class="h2">Сумма: <span data-value="order_total"></span> <i class="fa fa-rouble"></i> </p>
                </div>

            </div>
            <div class="row">
                <div class="col-md-3">
                    <button class="btn btn-primary" data-action="updateEditingOrder">Сохранить изменения</button>
                </div>
                <div class="col-md-3 col-md-offset-6 text-right">
                    <button class="btn btn-default" data-action="cancelEditingOrder">Отменить и закрыть</button>
                </div>
            </div>
        </div>
    </div>

</div>




<!-- Modal -->
<div class="modal fade" id="addOrderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Добавить новый заказ</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
                <button type="button" class="btn btn-primary">Сохранить изменения</button>
            </div>
        </div>
    </div>
</div>

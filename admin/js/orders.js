'use strict';

let $mode = -1;
let $orders = {};
let $editing_order = {};
let $products = [];


let __SoundEnabled = true;
let __Stat = {
    total: 0,
    earn: 0,
    count: 0
};

let $states = [[0,"Новый"],[1,"В обработке"],[2,"Завершен"],[3,"Отменен"]];

function updateOrderStatus(data){
    sendAjax("updateOrderStatus",data);
}

function parseState(stateId){
    let $state = "";
    switch (stateId){
        case 0: $state = ["new","Новый"];  break;
        case 1: $state = ["processing","В обработке"];  break;
        case 2: $state = ["completed","Выполнен"];  break;
        case 3: $state = ["cancelled","Отменен"];  break;
    }
    return $state;
}

function printOrder(_order, position){
    if ($mode != -1)
        if ( parseInt(_order.state) != $mode){return 0}

    let $totalled = 0;
    for (let j=0; j < _order.products.length; j++){
        let $total = 0;
        let $product = _order.products[j];
        if ($product.count != 0){
            $total = $total + ($product.price*$product.count);
            $totalled += $total;
          //  $(productsTable).append("<tr><td>" + $product.name + "</td><td>(" + $product.price +")</td><td>" + $product.count + "</td><td>" + $total + "</td></tr>");
        }
    }
   // $(order).append(productsTable);
    let stateRegulator = document.createElement('select');
    stateRegulator.classList.add('order_stateRegulator');
    stateRegulator.classList.add('form-control');
    stateRegulator.setAttribute('data-value',_order.id);

    let currentStateClass = 0;
    for(let $j=0; $j < $states.length; $j++ ){
        let $stateText = "";
        if ($states[$j][0] == _order.state){$stateText = "selected"; currentStateClass = $states[$j][0]; }
        $(stateRegulator).append("<option value='" + $states[$j][0]+"' "+$stateText+">"+$states[$j][1]+"</option>");
    }

    let order = document.createElement('div');
    order.classList.add("item");
    order.classList.add("clearfix");
    order.classList.add(parseState(_order.state)[0]);

    let date_now = new Date();
    date_now.setTime(Date.parse(_order.created));

    $(order).attr('data-value', _order.id);
    $(order).attr('data-id', position);
    $(order).append(
            "<div class='order-overlay'></div>" +
            "<div class='row'><div class='col-md-2'><small class='badge'>" + _order.id + "</small></div>" +
            "<div class='col-md-10 text-right'>" + _order.phone + "</div></div>"+
            "<div class='row'><div class='col-md-12 text-right'>Адрес: <small>" + _order.address + "</small></div></div>");

    $(order).append("<div class='row'>" +
        "<div class='col-md-6 mt-lg'>" +
        "<small><strong>"+ parseInt((  new Date - date_now) / 1000 / 60) +"</strong> мин назад </small>" +
        "</div>" +
        "<div class='col-md-6 text-right'><h4 class='mb-none'>"+$totalled+" руб</h4></div></div>"
    );


    let regulator_container = document.createElement('div');
        regulator_container.className = "col-md-4";
       // $(regulator_container).append(stateRegulator);

    $(order).append(regulator_container);

    if (_order.state == 2){
        // console.log($totalled);
        $totalled == undefined ? $totalled = 0 : "";
        isNaN($totalled) ? $totalled = 0 : "";
        __Stat.total += parseInt($totalled);
        __Stat.count ++;
    }

    _order = null;
    return order;
}
// вывод списка заказов
function printOrders(){
    __Stat = {
        total: 0,
        earn: 0,
        count: 0
    };
    let counter = 0;

    $('[id^="tab-"]').html("");


    for (let i=0; i < $orders.length ; i++) {
        let $order = $orders[i];
        $order = printOrder($order, counter);
        if ($order != 0){
            var container = "#tab-all";
            switch ($orders[i].state){
                case 0:
                    container+=",#tab-new";
                    playsound();
                    break;
                case 1:
                    container+=",#tab-working";
                    break;
                case 2:
                    container+=",#tab-completed";
                    break;
                case 3:
                    container+=",#tab-cancelled";
                    break;
            }
            $(container).append($order);counter++;
        }


        $($order).click(function () {
            let id = parseInt($(this).attr('data-id'));
           displayOrder(id);
        });

    }

    $('.order_stateRegulator').unbind();
    $('.order_stateRegulator').change(function(){
        updateOrderStatus({id: parseInt($(this).attr('data-value')), status: parseInt($(this).val())});
    });

    $('.cart-item .edit').click(function(){
        //  $('#modalOrder').removeClass('hidden');
        //  $('#orderNumber').html($(this).attr('data-value'));
    });
    __Stat.earn = __Stat.total * 0.1;
   // setTimeout("$('#order-loader').addClass('transparent')",900);
}

// отображение продукта для редактирования
function displayOrder(id){
    $('#order-details').removeClass('hidden');
    let order = $orders[id];
    $editing_order = $orders[id];
    refreshEditingOrder($editing_order);
}

// добавление продукта в редактируемый заказ
function addProduct(product) {
    // check if product already there
    let exist = false;
    for (let i=0; i < $editing_order.products.length; i++){
        if ($editing_order.products[i].id == product.id)
        {
            $editing_order.products[i].count++;
            exist = true;
        }
    }
    if (!exist){
        product.count = 1;
        $editing_order.products.push(product);
    }

    refreshEditingOrder($editing_order);
}

// удаление продукта из редактируемого заказа
function removeProduct(id) {
   for (let i =0; i <$editing_order.products.length; i++){
       $editing_order.products[i].id == id ? $editing_order.products.splice(i,1) : "";
   }
    refreshEditingOrder($editing_order);
}

function setupFields() {
    let inputs = $('#order-details input, #order-details textarea');

    $(inputs).unbind().on('change', function () {
       $editing_order[$(this).attr("data-value")]  = $(this).val();
       console.log($editing_order[$(this).attr("data-value")]);
    });
}

function setupNewOrderCreation() {
    $editing_order = {
        id: 0,
        state: 0,
        name: "",
        comment: "",
        phone: "",
        email: "",
        products: []
    };
    $('#order-details').removeClass('hidden');
    refreshEditingOrder($editing_order);
}

// обновление интерфейса редактирования заказа
function refreshEditingOrder(order) {
    setupFields();
    let container = $('#order-details [data-value="products"]');
    $(container).html('');
    let $totalled = 0;

    for (let j=0; j < order.products.length; j++){
        let $total = 0;
        let $product = order.products[j];

        if ($product.count != 0){

            $total = $total + ($product.price*$product.count);
            $totalled += $total;

            // формирование списка продуктов в редактируемом заказе
            let item = document.createElement('tr');
            let name = document.createElement('td');
                name.innerHTML = $product.name;
            let price = document.createElement('td');
                price.innerHTML = $product.price + " <i class='fa fa-rouble'></i></td>";
            let count = document.createElement('td');
                count.innerHTML = $product.count;

            let dec = document.createElement('td');
                let dec_btn = document.createElement('button');
                    dec_btn.classList.add("btn", "btn-default", "btn-sm");
                    dec_btn.innerHTML = "-";
                    dec_btn.onclick = function () {
                        $product.count --;
                        $product.count == 0 ? removeProduct($product.id) : "";
                        refreshEditingOrder($editing_order);
                    };
                dec.appendChild(dec_btn);

            let inc = document.createElement('td');
                let inc_btn = document.createElement('button');
                inc_btn.classList.add("btn", "btn-default", "btn-sm");
                inc_btn.innerHTML = "+";
                inc_btn.onclick = function () {
                    $product.count ++;
                    refreshEditingOrder($editing_order);
                };
                inc.appendChild(inc_btn);


            let total = document.createElement('td');
            total.innerHTML = $product.price*$product.count + " <i class='fa fa-rouble'></i>";

            let del = document.createElement('td');
            let del_btn = document.createElement('button');
                del_btn.classList.add("btn", "btn-default", "btn-sm");
                del_btn.innerHTML = "<i class='fa fa-trash-o'></i>";
                del_btn.onclick = function () { removeProduct($product.id); };
                del.appendChild(del_btn);

            item.append(name);
            item.append(price);
            item.append(dec);
            item.append(count);
            item.append(inc);
            item.append(total);
            item.append(del);

            $(container).append(item);
        }
    }

    order.total = $totalled;
    $('#order-details [data-value="order_state"]').html(parseState(order.state)[1]);
    $('#order-details [data-value="order_id"]').html(order.id);
    $('#order-details [data-value="name"]').val(order.name);
    $('#order-details [data-value="phone"]').val(order.phone);
    $('#order-details [data-value="address"]').val(order.address);
    $('#order-details [data-value="comment"]').val(order.comment);
    $('#order-details [data-value="date_created"]').html(order.created_time);
    $('#order-details [data-value="order_total"]').html(order.total);

    // смена статуса
    $('[data-toggle="orderState"]').click(function (e) {
        let thus = this;
        $('#order-details [data-value="order_state"]').html(thus.innerHTML);
        order.state = parseInt($(this).attr('data-value'));
        e.preventDefault();
    });

    // обработка изменения полей
    // обработка сохранения изменений в заказе
}

// получение всех продуктов
function getProducts() {
    $.ajax({
        method: "POST",
        url: "/engine/ajax.php",
        data: {
            action: 'getProducts'
        },
        success: function(response){
            let resp = JSON.parse(response);
            $products = resp;
            $('#addProductPanel .list').html('');

            for (let product of $products){
                let item = document.createElement('div');
                item.classList.add('item');

                let btn = document.createElement('button');
                btn.classList.add('btn');
                btn.classList.add('btn-default');
                btn.classList.add('btn-sm');
                btn.innerHTML = "+";
                btn.onclick = function () {  addProduct(product);  };

                let content = document.createElement('p');
                content.classList.add('content');
                content.innerHTML = product.name;

                content.appendChild(btn);
                item.appendChild(content);
                $('#addProductPanel .list').append(item);
            }
        }
    });

}

function convertProductsToJSON(order) {
    let products_array = [];

    for (let i=0; i < order.products.length; i++){
        let item = order.products[i];
        products_array.push([item.id, item.count])
    }

    return JSON.stringify(products_array);
}
// отправка изменений о заказе на сервер
function updateOrder(order) {
    // collect products data for database
    order.products_json = convertProductsToJSON(order);

    // determine - update or create a new one
    let action = 'updateOrder';
    if (order.id == 0){ action = 'createOrder'; }

    $.ajax({
        method: "POST",
        url: "/engine/ajax.php",
        data: {
            action: action,
            data: order
        },
        success: function(response){
            getOrders();
            $('#order-details').addClass('hidden');
        }
    });
}

function getOrders(){
    $('[data-action="getOrders"]').html('<i class="fa fa-spinner fa-spin"></i>');
    $.ajax({
        method: "POST",
        url: "/engine/ajax.php",
        data: {
            action: 'getOrders'
        },
        success: function(response){
            let resp = JSON.parse(response);
            //
            let counts = {
                new: 0,
                working: 0,
                completed: 0,
                cancelled: 0
            };

            for (let i=0; i < resp.length; i++){
                switch (resp[i].state){
                    case 0:
                        counts.new++;
                        break;
                    case 1:
                        counts.working++;
                        break;
                    case 2:
                        counts.completed++;
                        break;
                    case 3:
                        counts.cancelled++;
                        break;
                }
            }

            $('[data-value="count_new"]').html(counts.new);
            $('[data-value="count_working"]').html(counts.working);
            $('[data-value="count_completed"]').html(counts.completed);
            $('[data-value="count_cancelled"]').html(counts.cancelled);
            $('[data-value="orders_count"]').html(resp.length);

           $orders = resp;

            $('[data-action="getOrders"]').html('Получить заказы');
            printOrders();

        }
    });
}

function playsound(){
    if (__SoundEnabled){
       // var audio = $("#audio")[0];
       // audio.play();
    }
}

$(document).ready(function(){
    getOrders();
    getProducts();
   // setInterval('getOrders()', 25000);

    $('#tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    
    $('#navigation .nav-item').click(function(){
        $('#navigation .current').removeClass('current');
        $(this).addClass('current');
        $mode = parseInt($(this).attr('data-value'));
        printOrders();
    });

    $('[data-action="getOrders"]').click(function(){
        getOrders();
        $(this).html('<i class="fa fa-spinner fa-spin"></i>');
    });

    $('[data-action="toggleProductPanel"]').click(function () {
        $('#addProductPanel').toggleClass('hidden');
    });

    $('[data-action="updateEditingOrder"]').click(function () {
        updateOrder($editing_order);
    });

    $('[data-action="newOrder"]').click(function () {
       setupNewOrderCreation();
    });

    $('[data-action="cancelEditingOrder"]').click(function () {
        $editing_order = {};
        $('#order-details').addClass('hidden');
    });
});
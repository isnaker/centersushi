function changeSoundMode(){
    if (document.getElementById('sound').checked){
        setCookie("sound", "enabled", "Mon, 01-Jan-2020 00:00:00 GMT", "/");
        // $('.sound_title').html('Звук включен');
        return true;
    }
    else {
        setCookie("sound", "disabled", "Mon, 01-Jan-2020 00:00:00 GMT", "/");
        // $('.sound_title').html('Звук выключен');
        return false;
    }
}

$(function(){
    $('#section_switcher').click(function(){
        $('#content').toggleClass('slided');
    });
});
$(document).ready(function(){
    $('img').error(function(){
        $(this).attr("src",'/img/products/product-example.jpg');
    });

    $('#mobile_nav_toggle').click(function(){
        $('#mobile_nav_container').addClass('active');
        $('#mobile_nav_toggle').addClass('hidden');
    });

    $('#mobile_nav_container .toggler').click(function(){
        $('#mobile_nav_container').removeClass('active');
        $('#mobile_nav_toggle').removeClass('hidden');
    });
});

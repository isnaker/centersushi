var cart = {}

function sendAjax(action, _data, sender){
    var _response = 0;
    $.ajax({
        type: 'POST',
        url: '/engine/ajax.php',
        data: {
            action: action,
            data : _data
        },
        success: function(data){
            var _response;
            switch (action){
                case "quickAddToCart": getCart('printProductList()'); _response = 1; break;
                case "addToCart": getCart('printProductList()'); _response = 1; break;
                case "clearCart": getCart('printProductList()'); break;
                case "updateCart": getCart('printProductList()'); break;
                case "createOrder": response = data; printResult(response);  break;
                case "getOrders": $orders = JSON.parse(data); $orders.sort(); $orders.reverse(); printOrders();  break;
                case "updateOrderStatus": console.log("Попытка сменить статус!"); sendAjax("getOrders");    break;
            }
        },
        timeout: function(){
            return 0;
        },
        error: function(){
            return 0;
        }
    });

    return _response;
}

function getCart(callback){
    var data;
    $.ajax({
        type: 'POST',
        url: '/engine/ajax.php',
        data: {
            action: 'getCart'
        },
        success: function(data){
            cart = JSON.parse(data);
            setTimeout( function() { $("[data-value='cart-count']").html(cart.items.length) }, 1000);
            if (callback){
                var cback = new Function(callback);
                switch (callback){
                    case "printProductList()": cback(); break;
                }
            }
        }
    });
}

function eraseCartContainers(){
    $('#product_list').html("");
    $('#total-container').remove();
}

function addToCart(id,count){
    if (!count){count=1}
    $('[data-value="cart-count"]').html('<i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i>');
    var response = sendAjax('addToCart',{productId:parseInt(id),productCount:count});
    return response;
}

function quickAddToCart(id,count){
    if (!count){count=1}
    $('[data-value="cart-count"]').html('<i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i>');
    var response = sendAjax('quickAddToCart',{productId:parseInt(id),productCount:count});
    return response;
}

function removeProduct(id){
    sendAjax("updateCart", {id:id,delta:0});
}

function eraseCart(){
    sendAjax('clearCart');
}

function setCookie (name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

$(document).ready(function(){
    getCart();
    $('.quickAddToCart').click(function(){
        var prevHtml = $(this).html();
       var id = $(this).attr('data-value');
        addToCart(id,1);

        $(this).html('в корзине!');

        var i = this;
        setTimeout(function(){$(i).html(prevHtml)}, 1500);
        return false;
    });
});

<?php
require('engine/config.php');
$app = new App();
$db = new DB();
?>

<html>
<head>
    <title>Доставка суши и ролов в Коломне. Акции. Центр Суши</title>
    <link rel="stylesheet" href="css/framework.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">

    <link rel="stylesheet" href="css/product.css" type="text/css">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="css/product.preview.css" type="text/css">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="keywords" content="Центр Суши, суши, роллы, заказать, заказать роллы Коломна, роллы Коломна, суши Коломна, заказать суши Коломна, заказать роллы, заказать суши, Суши ,доставка, суши, роллы, салаты, сашими, сеты, горячее, супы, десерты, напитки, Коломна, японская кухня, блюда, суши в коломне, доставка суши в коломне, заказать суши в Коломне"/>
    <meta name="description" content="Бесплатная доставка суши и роллов на дом в Коломне. Японские блюда от «Центр Суши» с доставкой."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/basic.js"></script>
    <script type="text/javascript" src="js/cart.js"></script>
    <style type="text/css">
        h1,h2,strong {font-weight: 400}

        h1 {margin-bottom: 8px; font-family: 'Roboto' !important; font-weight: 400}
        .bigger {
            display: block;
            font-size: 28px;
            margin-left: 25px;
        }
        .action {
            background: #ffffff;
            box-shadow: 0 1px 4px rgba(0,0,0,0.66);
             float: left;
            display: inline-block;
            padding: 10px 18px;
            margin: 8px;
            max-width: 43%;
        }

        .h1 { font-size: 1.62em }

        .mt-none {margin-top: 0}
        .mb-none {margin-bottom: 0}
        .img-responsive {max-width: 100%}

        .big {font-size: 1.2em}

        .red { color: #c50700; }
        #map-canvas {
            width: 100%;
            margin: 20px 0 8px 0;
            height: 270px;
            background: white;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $('nav a[href="actions.php"]').addClass('current');
            $('.blocklist li').hide();
            $("#left-column .expander").click(function(){
                $(this).toggleClass('expanded');
                var p = $(this).parent().next();
                if ($(this).hasClass('expanded')){
                    $(p).find('li').show(300);
                } else {
                    $(p).find('li').hide(500);
                }
            });
        });
    </script>
 </head>

<body>
<?php include_once('header.php'); ?>
<div id="content">
    <div id="left-column">
        <div class="accordion">
            <?php
            $categories = $db->getCategories("",true);
            $products = $db->getProducts();

            foreach ($categories as $category){
                $products = $db->getProducts("`category` = $category->id","","`name`");
                if (count($products) > 0){
                    print ("<span class='blocklist-head'><span class='expander'>$category->name</span> <a href='category.php?category=".$category->id."'>Все</a> </span><ul class='blocklist'>");
                    foreach ($products as $product){
                        print "<li><a href='product.php?id=$product->id'>".$product->name."<span data-value='$product->id' class='quickAddToCart'>+</span></a></li>";
                    }
                    print ("</ul>");
                }
            }
            ?>
        </div>
    </div>
    <div id="right-column">
        <div class='breadcrumbs'>
            <a href='index.php'>Центр суши</a> >
            <span>Наши акции</span>
        </div>
        <div id="main">
            <h1>Акции "Центр Суши"</h1>
            <h3>Действует накопительная система скидок постоянным клиентам.</h3>
            <div class="action">
                <p class="h1 mb-none mt-none">День Рождения</p>
                <p>В День Вашего Рождения 20% на ВСЕ кроме сэтов и напитков ( И после в течении 10ти календарных дней) </p>
                <img src="/img/slides/birthday.jpg" alt="" class="img-responsive">
            </div>
            <div class="action">
                <p class="h1 mb-none mt-none">Скидка в будни</p>
                <p>По будням с 11-00 до 16-00 20% на ВСЕ кроме сэтов и напитков</p>
                <img src="/img/slides/diner.jpg" alt="" class="img-responsive">
            </div>
            <div class="action">
                <p class="h1 mb-none mt-none">Калифорния в подарок!</p>
                <p>От заказа (роллов) на 1000р классическая Калифорния в подарок</p>
                <img src="/img/slides/1000.jpg" alt="" class="img-responsive">
            </div>
            <div class="action">
                <p class="h1 mb-none mt-none">Острый Лосось в подарок!</p>
                <p>От заказа (роллов) на 1500р ролл Острый Лосось в подарок </p>
                <img src="/img/slides/1500.jpg" alt="" class="img-responsive">
            </div>
            <div class="action">
                <p class="h1 mb-none mt-none">Сэт №1 в подарок!</p>
                <p>От заказа(роллов) на 2000р Обновленный Сэт №1 из нового меню в подарок</p>
                <img src="/img/slides/2000.jpg" alt="" class="img-responsive">
            </div>
            <div class="action">
                <p class="h1 mb-none mt-none">20% скидка в начале недели!</p>
                <p>Пн/Вт/Ср Скидка 20% на все сэты роллов ( кроме 3,5,7) при самовывозе
                    При заказе трёх сэтов 20% и бесплатная доставка !</p>
                <img src="/img/slides/monday.jpg" alt="" class="img-responsive">
            </div>
            <div class="action">
                <p class="h1 mb-none mt-none">Скидка при самовывозе</p>
                <p>С понедельника по субботу скидка 10% самовывозом на всё ( кроме сэтов и напитков)</p>
                <p>Каждое воскресенье скидка 20% (кроме сэтов и напитков ) при самовывозе.</p>
            </div>
            <div class="clearfix">Акции не суммируются</div>
        </div>
    </div>
 </div>

<?php include_once('footer.php'); ?>
</body>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=ru"></script>
<script type="text/javascript">
    function initialize(){
        var centr = new google.maps.LatLng(55.101070, 38.748536);
        var opts = {
            zoom: 12,
            center: centr,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById("map-canvas"), opts);
        var image = 'img/icon.png';
        var marker = new google.maps.Marker({
            map: map,
            position: centr
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
</html>
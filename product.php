<?php
session_start();

require('engine/config.php');
$id = $_GET['id'];
$app = new App();
$db = new DB();
$product_ = $db->getProduct($id);
$categoryName = $db->getCategoryNameById($product_->category);
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php  print($categoryName." : ".$product_->name); ?> - Центр Суши</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/product.css" type="text/css">
    <link rel="stylesheet" href="css/product.preview.css" type="text/css">
    <link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="keywords" content="Центр Суши, суши, роллы, заказать, заказать роллы Коломна, роллы Коломна, суши Коломна, заказать суши Коломна, заказать роллы, заказать суши, Суши ,доставка, суши, роллы, салаты, сашими, сеты, горячее, супы, десерты, напитки, Коломна, японская кухня, блюда, суши в коломне, доставка суши в коломне, заказать суши в Коломне"/>
    <meta name="description" content="Бесплатная доставка суши и роллов на дом в Коломне. Японские блюда от «Центр Суши» с доставкой."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="js/basic.js"></script>
    <script type="text/javascript" src="js/cart.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.blocklist li').hide();
            $("#left-column .expander").click(function(){
                $(this).toggleClass('expanded');
                var p = $(this).parent().next();
                if ($(this).hasClass('expanded')){
                    $(p).find('li').show(300);
                } else {
                    $(p).find('li').hide(500);
                }
            });
            $('nav a[href="category.php"]').addClass('current');

            $('#menu_controller').click(function(){
                $('#left-column .accordion').toggleClass('showed');
            });

            $('#addToCartButton').click(function(){
               var value = $(this).attr('data-value');
                if (value)
                {
                    var q = addToCart(value,1);

                    $('#addToCartButton_response').html('Добавлено в корзину!').addClass('visible');
                   setTimeout( function() { $('#addToCartButton_response').removeClass('visible'); }, 3000 );

                }
            });
        });
    </script>
</head>

<body>
<?php include_once('header.php'); ?>
<div id="content">
    <div id="left-column">
        <div class="accordion">
        <?php
        $categories = $db->getCategories("",true);
        $products = $db->getProducts();

        foreach ($categories as $category){
            $products = $db->getProducts("`category` = $category->id","","`id`,`name`");
            if (count($products) > 0){
                print ("<span class='blocklist-head'><span class='expander'>$category->name</span> <a href='category.php?category=".$category->id."'>Все</a> </span><ul class='blocklist'>");
                foreach ($products as $product){
                    print "<li><a href='product.php?id=$product->id'>".$product->name."<span data-value='$product->id' class='quickAddToCart'>+</span></a></li>";
                }
                print ("</ul>");
            }
        }
        ?>
            </div>
    </div>
    <div id="right-column">
        <div class='breadcrumbs'>
            <a href='index.php'>Центр суши</a> >
            <a href='category.php'>Меню</a> >
            <a href="category.php?category=<?php print($product_->category); ?>"><?php print $db->getCategoryNameById($product_->category); ?></a> >
            <span><?php print $product_->name; ?></span>
        </div>
    <div id="main">
        <?php $product_->writeModal(); ?>

        <div id="how_to_order" class="floating_block">
            <p>Также Вы можете сделать заказ, позвонив по телефону<strong><?= $app->phone;  ?></strong></p>
        </div>
    </div>

        <div class="clearfix"></div>
        <h2>Самые популярные</h2>
        <?php
        $i = 0;
        $prod = $db->getPopular(3, $product_->category);
        foreach ($prod as $product){
            $i++;
            $product->write();
            if ($i % 3 == 0){print "<div class='clearfix'></div>";}
        }
        ?>
    </div>
</div>

<?php include_once('footer.php'); ?>
</body>
</html>
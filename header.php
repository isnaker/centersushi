<header>
    <div id="header-container">
    <a id="logo" href="index.php"> <img alt="Центр суши" src="img/logo.png" width="120"> </a>
    <div class="header-block" id="header-promo">
        <h2><?php $a = new App(); ?><a href="tel:<?php $a->phone(); ?>"> <?php $a->phone(); ?></a></h2>
        <h3 id="header-annotation">Доставка суши и пиццы в Коломне</h3>
        <h4 class="small">Минимальный заказ - 450 руб.</h4>
    </div>
    <div class="header-block header-contacts">
        <h2 class="text-centered">Время работы:</h2>

        <div class="worktime">
            <div class="box25p">
                <h2 class="time">11:00 - 23:00</h2>
                <h4 class="small">Понедельник - Четверг</h4>
            </div>

            <div class="box25p">
                <h2 class="time">11:00 - 00:00</h2>
                <h4 class="small">Пятница и суббота</h4>
            </div>

            <div class="box25p">
                <h2 class="time">11:00 - 23:00</h2>
                <h4 class="small">Воскресенье</h4>
            </div>


        </div>

    </div>
        <div class="header-block" id="cart">
            <h3>
                <a href="cart.php">
                    <img class="cart-icon" src="img/basket-icon.png" alt="Корзина"> Корзина
                    <span id="minicart_productCount">(<span data-value="cart-count"><i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></span>)</span></a></h3>

            <div id="weAccept">
                <p class="nopadding"> Мы принимаем</p>
                <img src="/img/visa.png" alt="Мы принимаем Visa Mastercard">
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</header>
<nav>
    <a href="index.php">Главная</a>
    <a href="category.php">Меню</a>
    <a href="actions.php">Акции</a>
    <a style="display: none" href="news.php">Новости</a>
    <a style="display: none" href="testimonials.php">Отзывы</a>
    <a href="contacts.php">Контакты</a>
</nav>
<div class="clearfix"></div>
<span id="menu_controller"><img src="img/icon-menu.png" alt="показать меню"> Показать меню</span>

<div id="mobile_nav_container">
    <div class="content">

        <div class="toggler"><i class="fa fa-arrow-circle-left fa-2x color-white"></i></div>
        <div class="float_cart"><a href="/cart.php" class="color-white"> <i class="fa fa-shopping-bag"></i> <span data-value="cart-count"></span> </a></div>
        <img src="/img/logo.png" alt="Центр Суши логотип">
        <div class="menu items">
            <?php

            $categories = $db->getCategories("",true);
            foreach ($categories as $category){
                $products = $db->getProducts("`category` = $category->id","","`name`",false);
                if (count($products) > 0){ ?>
                    <a class="item color-white" href='category.php?category="<?= $category->id ?>"'><?= $category->name ?></a>
                 <?php }
            }
            ?>
        </div>

        <div class="footer color-white">
            <div><small>Навигация</small></div>

            <div class="mt-lg nav">
                <a href="/">Главная</a>
                <a href="/menu.php">Меню</a>
                <a href="/actions.php">Акции</a>
                <a href="/contacts.php">Контакты</a>
            </div>

            <div class="mt-xlg">Центр Суши, <?= date("Y") ?></div>
        </div>

    </div>
</div>
<div id="mobile_nav_toggle"><i class="fa fa-bars"></i> </div>
﻿<?php
include('engine/config.php');
$db = new DB();

function sortl($a, $b, $sortByField = 'id') {if($a->id === $b->id)return 0;return $a->id < $b->id ? 1 : -1;}
//uasort($products, 'myCmp');  // сортировка элементов
$counter = 0;


?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Доставка пиццы и роллов в Коломне. Заказать суши в Коломне. Заказать пиццу в Коломне. Центр Суши</title>
    <link rel="stylesheet" href="/css/style.css" type="text/css">

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name='yandex-verification' content='48de752d2a7ce431' />
    <meta name="keywords" content="Центр Суши, суши, роллы, заказать, заказать роллы Коломна, роллы Коломна, суши Коломна, заказать суши Коломна, заказать роллы, заказать суши, Суши ,доставка, суши, роллы, салаты, сашими, сеты, горячее, супы, десерты, напитки, Коломна, японская кухня, блюда, суши в коломне, доставка суши в коломне, заказать суши в Коломне"/>
    <meta name="description" content="Японские блюда от «Центр Суши» с доставкой. Доставка суши и роллов на дом в Коломне."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    

    <style>
        body {
            background: url('img/bluredpizza.jpg') no-repeat;
            background-position: center center;
            background-size: 140%;
            background-color: #f7f6f5;
        }
    </style>
</head>

<body>
<?php include_once('header.php'); ?>
<div id="content">

    <div id="slider">
        <div class="flexslider">
            <ul class="slides">
                <li>
                    <img src="/img/slides/monday.jpg" />
                </li>
                <li>
                    <img src="/img/slides/diner.jpg" />
                </li>
                <li>
                    <img src="/img/slides/birthday.jpg" />
                </li>
                <li>
                    <img src="/img/slides/1000.jpg" />
                </li>
                <li>
                    <img src="/img/slides/1500.jpg" />
                </li>
                <li>
                    <img src="/img/slides/2000.jpg" />
                </li>
            </ul>
        </div>
    </div>

    <div id="left-column">
        <div class="accordion">
        <?php
        foreach ($categories as $category){
            $products = $db->getProducts("`category` = $category->id","","`id`,`name`",false);
            if (count($products) > 0){
                print ("<span class='blocklist-head'><span class='expander'>$category->name</span> <a href='category.php?category=".$category->id."'>Все</a> </span><ul class='blocklist'>");
                foreach ($products as $product){
                    print "<li><a href='product.php?id=$product->id'>".$product->name."<span data-value='$product->id' class='quickAddToCart'>+</span></a></li>";
                }
                print ("</ul>");
            }
        }
        ?>
        </div>
        <div class="leftside_promo">
            <img src="img/drivers.png" alt="Заказать суши в Коломне - приглашаем водителей">
        </div>
    </div>
    <div id="right-column">
        <div id="main">
            <h1 style="font-size: 14px">Заказать роллы в Коломне. Центр Суши.</h1>


        <div id="groups">

            <div class="group-container">
                <span class="group-heading">Популярная пицца <img src="img/new_item_icon.png" alt="новое">
                <a href="category.php?category=12" style="float: right; color: wheat">Смотреть все пиццы >> </a>
                </span>
                <?php
                $i = 0;
                $prod = $db->getPopular(6,12);
                foreach ($prod as $product){
                    $i++;
                    $product->write();
                    if ($i % 3 == 0){print "<div class='clearfix'></div>";}
                }
                ?>
                <div class="clearfix"></div>
            </div>

            <div class="group-container">
                <span class="group-heading">Самые популярные</span>
            <?php
                $i = 0;
                $prod = $db->getPopular(6);
                foreach ($prod as $product){
                    $i++;
                    $product->write();
                    if ($i % 3 == 0){print "<div class='clearfix'></div>";}
                }
            ?>
                <div class="clearfix"></div>
            </div>

            <div class="group-container">
                <span class="group-heading">Подборка суши</span>
                <?php
                $sushi = $db->getProductGroup("index_sushi");
                foreach ($sushi as $sush){
                    $sush->write();
                }
                ?>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
    </div>
    <div class="clearfix"></div>

</div>

<?php include_once('footer.php'); ?>


<link type="text/css" rel="stylesheet" href="css/flexslider.css">
<link rel="stylesheet" href="css/product.css" type="text/css">


<script type="text/javascript" src="/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/js/basic.js"></script>
<script type="text/javascript" src="/js/cart.js"></script>
<script type="text/javascript" src="/js/jquery.easing.js"></script>
<script type="text/javascript" src="/js/modernizr.js"></script>
<script type="text/javascript" src="/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/js/jquery.flexslider-min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.blocklist li').hide();
        $("#left-column .expander").click(function(){
            $(this).toggleClass('expanded');
            var p = $(this).parent().next();
            if ($(this).hasClass('expanded')){
                $(p).find('li').show(300);
            } else {
                $(p).find('li').hide(500);
            }
        });

        $('nav a[href="index.php"]').addClass('current');
        $('.flexslider').flexslider({ animation: "slide" });

        $('#menu_controller').click(function(){
            $('#left-column .accordion').toggleClass('showed');
        });
    });
</script>

</body>

</html>